use crate::article_view::{ArticleTheme, ArticleView};
use crate::i18n::i18n;
use chrono::Utc;
use glib::{clone, subclass::*};
use gtk4::{prelude::*, subclass::prelude::*, CompositeTemplate, GestureClick, ListBox, ListBoxRow, Popover, Widget};
use lazy_static::lazy_static;
use news_flash::models::{ArticleID, FatArticle, FeedID, Marked, Read};
use webkit2gtk::{traits::WebViewExt, WebView};

lazy_static! {
    pub static ref SIGNALS: Vec<Signal> = vec![Signal::builder(
        "article-theme-changed",
        &[i32::static_type().into()],
        <()>::static_type().into(),
    )
    .build()];
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/theme_chooser.ui")]
    pub struct ThemeChooser {
        #[template_child]
        pub theme_list: TemplateChild<ListBox>,

        #[template_child]
        pub default_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub default_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub default_view: TemplateChild<WebView>,

        #[template_child]
        pub spring_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub spring_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub spring_view: TemplateChild<WebView>,

        #[template_child]
        pub midnight_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub midnight_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub midnight_view: TemplateChild<WebView>,

        #[template_child]
        pub parchment_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub parchment_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub parchment_view: TemplateChild<WebView>,

        #[template_child]
        pub gruvbox_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub gruvbox_click_controller: TemplateChild<GestureClick>,
        #[template_child]
        pub gruvbox_view: TemplateChild<WebView>,
    }

    impl Default for ThemeChooser {
        fn default() -> Self {
            Self {
                theme_list: TemplateChild::default(),

                default_row: TemplateChild::default(),
                default_click_controller: TemplateChild::default(),
                default_view: TemplateChild::default(),

                spring_row: TemplateChild::default(),
                spring_click_controller: TemplateChild::default(),
                spring_view: TemplateChild::default(),

                midnight_row: TemplateChild::default(),
                midnight_click_controller: TemplateChild::default(),
                midnight_view: TemplateChild::default(),

                parchment_row: TemplateChild::default(),
                parchment_click_controller: TemplateChild::default(),
                parchment_view: TemplateChild::default(),

                gruvbox_row: TemplateChild::default(),
                gruvbox_click_controller: TemplateChild::default(),
                gruvbox_view: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThemeChooser {
        const NAME: &'static str = "ThemeChooser";
        type ParentType = Popover;
        type Type = super::ThemeChooser;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ThemeChooser {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }

        fn constructed(&self, pop: &Self::Type) {
            let imp = imp::ThemeChooser::from_instance(pop);

            let mut demo_article = FatArticle {
                article_id: ArticleID::new("demo"),
                title: None,
                author: None,
                feed_id: FeedID::new("demo_feed"),
                direction: None,
                date: Utc::now().naive_utc(),
                synced: Utc::now().naive_utc(),
                marked: Marked::Unmarked,
                unread: Read::Unread,
                url: None,
                summary: None,
                html: None,
                scraped_content: None,
                plain_text: None,
                thumbnail_url: None,
            };

            super::ThemeChooser::prepare_theme_selection(
                &*imp.default_row,
                &imp.default_click_controller,
                &imp.default_view,
                &mut demo_article,
                ArticleTheme::Default,
                &i18n("Default"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*imp.spring_row,
                &imp.spring_click_controller,
                &imp.spring_view,
                &mut demo_article,
                ArticleTheme::Spring,
                &i18n("Spring"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*imp.midnight_row,
                &imp.midnight_click_controller,
                &imp.midnight_view,
                &mut demo_article,
                ArticleTheme::Midnight,
                &i18n("Midnight"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*imp.parchment_row,
                &imp.parchment_click_controller,
                &imp.parchment_view,
                &mut demo_article,
                ArticleTheme::Parchment,
                &i18n("Parchment"),
            );
            super::ThemeChooser::prepare_theme_selection(
                &*imp.gruvbox_row,
                &imp.gruvbox_click_controller,
                &imp.gruvbox_view,
                &mut demo_article,
                ArticleTheme::Gruvbox,
                &i18n("Gruvbox"),
            );

            imp.theme_list
                .connect_row_activated(clone!(@weak pop => @default-panic, move |_list, row| {
                    pop.popdown();

                    let theme = ArticleTheme::from_str(row.widget_name().as_str()).to_i32();

                    pop.emit_by_name::<()>("article-theme-changed", &[&theme]);
                }));
        }
    }

    impl WidgetImpl for ThemeChooser {}

    impl PopoverImpl for ThemeChooser {}
}

glib::wrapper! {
    pub struct ThemeChooser(ObjectSubclass<imp::ThemeChooser>)
        @extends Widget, Popover;
}

impl ThemeChooser {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    fn prepare_theme_selection(
        row: &ListBoxRow,
        click_controller: &GestureClick,
        view: &WebView,
        article: &mut FatArticle,
        theme: ArticleTheme,
        name: &str,
    ) {
        click_controller.connect_pressed(clone!(@weak row => @default-panic, move |_gesture, times, _x, _y| {
            if times != 1 {
                return;
            }
            row.emit_activate();
        }));
        article.title = Some(name.to_owned());
        let prefer_dark_theme = libadwaita::StyleManager::default()
            .map(|sm| sm.is_dark())
            .unwrap_or(false);
        let html = ArticleView::build_article_static(
            "theme_preview",
            article,
            "Feed Name",
            None,
            Some(theme),
            Some(10240),
            Some(false),
            false,
            prefer_dark_theme,
        );
        view.load_html(&html, None);
    }
}
