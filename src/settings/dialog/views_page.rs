use super::theme_chooser::ThemeChooser;
use crate::app::{Action, App};
use crate::article_view::ArticleTheme;
use crate::settings::article_list::GArticleOrder;
use crate::util::Util;
use glib::clone;
use gtk4::ClosureExpression;
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, FontButton, GestureClick, Inhibit, Label, Switch, Widget,
};
use libadwaita::{prelude::*, subclass::prelude::*, ActionRow, ComboRow, PreferencesPage};
use libadwaita::{EnumListItem, EnumListModel};
use parking_lot::RwLock;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/settings_views.ui")]
    pub struct SettingsViewsPage {
        #[template_child]
        pub filter_feeds_switch: TemplateChild<Switch>,
        #[template_child]
        pub article_order_row: TemplateChild<ComboRow>,
        #[template_child]
        pub show_thumbs_switch: TemplateChild<Switch>,
        #[template_child]
        pub article_theme_label: TemplateChild<Label>,
        #[template_child]
        pub article_theme_click: TemplateChild<GestureClick>,
        #[template_child]
        pub allow_selection_switch: TemplateChild<Switch>,
        #[template_child]
        pub font_row: TemplateChild<ActionRow>,
        #[template_child]
        pub font_button: TemplateChild<FontButton>,
        #[template_child]
        pub use_system_font_switch: TemplateChild<Switch>,

        pub article_theme_popover: RwLock<Option<ThemeChooser>>,
    }

    impl Default for SettingsViewsPage {
        fn default() -> Self {
            Self {
                filter_feeds_switch: TemplateChild::default(),
                article_order_row: TemplateChild::default(),
                show_thumbs_switch: TemplateChild::default(),
                article_theme_label: TemplateChild::default(),
                article_theme_click: TemplateChild::default(),
                allow_selection_switch: TemplateChild::default(),
                font_row: TemplateChild::default(),
                font_button: TemplateChild::default(),
                use_system_font_switch: TemplateChild::default(),

                article_theme_popover: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SettingsViewsPage {
        const NAME: &'static str = "SettingsViewsPage";
        type ParentType = PreferencesPage;
        type Type = super::SettingsViewsPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SettingsViewsPage {}

    impl WidgetImpl for SettingsViewsPage {}

    impl PreferencesPageImpl for SettingsViewsPage {}
}

glib::wrapper! {
    pub struct SettingsViewsPage(ObjectSubclass<imp::SettingsViewsPage>)
        @extends Widget, PreferencesPage;
}

impl SettingsViewsPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::SettingsViewsPage::from_instance(self);

        // -----------------------------------------------------------------------
        // feed list show only relevant
        // -----------------------------------------------------------------------
        imp.filter_feeds_switch
            .set_state(App::default().settings().read().get_feed_list_only_show_relevant());

        // -----------------------------------------------------------------------
        // article list order
        // -----------------------------------------------------------------------
        let params: &[gtk4::Expression] = &[];
        let article_order_closure = ClosureExpression::new(params, |v: &[glib::Value]| {
            let e = v[0].get::<EnumListItem>().unwrap();
            let article_order = GArticleOrder::from_u32(e.value() as u32).to_news_flash();
            return article_order.to_str().to_owned();
        });
        imp.article_order_row.set_expression(Some(&article_order_closure));
        imp.article_order_row
            .set_model(Some(&EnumListModel::new(GArticleOrder::static_type())));
        imp.article_order_row.set_selected(
            GArticleOrder::from_news_flash(App::default().settings().read().get_article_list_order()).to_u32(),
        );

        // -----------------------------------------------------------------------
        // article list show thumbnails
        // -----------------------------------------------------------------------
        imp.show_thumbs_switch
            .set_state(App::default().settings().read().get_article_list_show_thumbs());

        // -----------------------------------------------------------------------
        // article view theme
        // -----------------------------------------------------------------------
        imp.article_theme_label
            .set_label(App::default().settings().read().get_article_view_theme().name());
        let article_theme_popover = ThemeChooser::new();
        article_theme_popover.set_parent(&*imp.article_theme_label);
        imp.article_theme_popover.write().replace(article_theme_popover);

        // -----------------------------------------------------------------------
        // article view allow selection
        // -----------------------------------------------------------------------
        imp.allow_selection_switch
            .set_state(App::default().settings().read().get_article_view_allow_select());

        // -----------------------------------------------------------------------
        // article view fonts
        // -----------------------------------------------------------------------
        let have_custom_font = App::default().settings().read().get_article_view_font().is_some();
        imp.font_row.set_sensitive(have_custom_font);
        imp.font_button.set_sensitive(have_custom_font);
        if let Some(font) = App::default().settings().read().get_article_view_font() {
            imp.font_button.set_font(&font);
        }
        imp.use_system_font_switch.set_state(!have_custom_font);

        self.setup_feed_list();
        self.setup_article_list();
        self.setup_article_view();
    }

    fn theme_pop(&self) -> ThemeChooser {
        let imp = imp::SettingsViewsPage::from_instance(self);
        imp.article_theme_popover
            .read()
            .clone()
            .expect("SettingsViewsPage not initialized")
    }

    fn setup_feed_list(&self) {
        let imp = imp::SettingsViewsPage::from_instance(self);

        imp.filter_feeds_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_feed_list_only_show_relevant(is_set)
                .is_err()
            {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'show relevant feeds'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateSidebar);
            Inhibit(false)
        });
    }

    fn setup_article_list(&self) {
        let imp = imp::SettingsViewsPage::from_instance(self);

        imp.article_order_row.connect_selected_notify(|row| {
            let selected_index = row.selected();
            let new_order = GArticleOrder::from_u32(selected_index);
            if App::default()
                .settings()
                .write()
                .set_article_list_order(new_order.to_news_flash())
                .is_ok()
            {
                Util::send(Action::UpdateArticleList);
            } else {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'article order'.".to_owned(),
                ));
            }
        });

        imp.show_thumbs_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_article_list_show_thumbs(is_set)
                .is_err()
            {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'show thumbnails'.".to_owned(),
                ));
            }
            Util::send(Action::UpdateArticleList);
            Inhibit(false)
        });
    }

    fn setup_article_view(&self) {
        let imp = imp::SettingsViewsPage::from_instance(self);
        let article_theme_popover = self.theme_pop();

        imp.article_theme_click.connect_pressed(clone!(
            @weak article_theme_popover => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            article_theme_popover.popup();
        }));

        let article_theme_label = imp.article_theme_label.get();
        article_theme_popover.connect_local("article-theme-changed", false, move |val| {
            let article_theme_i32 = val[1].get::<i32>().expect("The value needs to be of type `i32`.");
            let article_theme = ArticleTheme::from_i32(article_theme_i32);
            if App::default()
                .settings()
                .write()
                .set_article_view_theme(&article_theme)
                .is_err()
            {
                Util::send(Action::ErrorSimpleMessage("Failed to set article theme.".into()));
            }
            article_theme_label.set_label(article_theme.name());
            Util::send(Action::RedrawArticle);
            None
        });

        imp.allow_selection_switch.connect_state_set(|_switch, is_set| {
            if App::default()
                .settings()
                .write()
                .set_article_view_allow_select(is_set)
                .is_ok()
            {
                Util::send(Action::RedrawArticle);
            } else {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'allow article selection'.".to_owned(),
                ));
            }
            Inhibit(false)
        });

        imp.font_button.connect_font_set(|button| {
            let font = match button.font() {
                Some(font) => Some(font.to_string()),
                None => None,
            };
            if App::default().settings().write().set_article_view_font(font).is_ok() {
                Util::send(Action::RedrawArticle);
            } else {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'article font'.".to_owned(),
                ));
            }
        });

        let font_button = imp.font_button.get();
        let font_row = imp.font_row.get();
        imp.use_system_font_switch.connect_state_set(move |_switch, is_set| {
            let font = if is_set {
                None
            } else if let Some(font_name) = font_button.font() {
                Some(font_name.to_string())
            } else {
                None
            };
            font_button.set_sensitive(!is_set);
            font_row.set_sensitive(!is_set);
            if App::default().settings().write().set_article_view_font(font).is_ok() {
                Util::send(Action::RedrawArticle);
            } else {
                Util::send(Action::ErrorSimpleMessage(
                    "Failed to set setting 'use system font'.".to_owned(),
                ));
            }
            Inhibit(false)
        });
    }
}
