use glib::Enum;
use news_flash::models::ArticleOrder;
use serde::{Deserialize, Serialize};
use std::default::Default;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct ArticleListSettings {
    pub order: ArticleOrder,
    #[serde(default)]
    pub show_thumbnails: bool,
}

impl Default for ArticleListSettings {
    fn default() -> Self {
        ArticleListSettings {
            order: ArticleOrder::NewestFirst,
            show_thumbnails: true,
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, Eq, PartialEq, Enum)]
#[repr(u32)]
#[enum_type(name = "GArticleOrder")]
pub enum GArticleOrder {
    NewestFirst,
    OldestFirst,
}

impl GArticleOrder {
    pub fn from_news_flash(order: ArticleOrder) -> Self {
        match order {
            ArticleOrder::NewestFirst => Self::NewestFirst,
            ArticleOrder::OldestFirst => Self::OldestFirst,
        }
    }

    pub fn to_news_flash(&self) -> ArticleOrder {
        match self {
            Self::NewestFirst => ArticleOrder::NewestFirst,
            Self::OldestFirst => ArticleOrder::OldestFirst,
        }
    }

    pub fn from_u32(v: u32) -> Self {
        match v {
            0 => Self::NewestFirst,
            1 => Self::OldestFirst,
            _ => Self::NewestFirst,
        }
    }

    pub fn to_u32(&self) -> u32 {
        match self {
            Self::NewestFirst => 0,
            Self::OldestFirst => 1,
        }
    }
}
