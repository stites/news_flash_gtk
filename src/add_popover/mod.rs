mod add_category_widget;
mod add_feed_widget;
mod add_tag_widget;

use self::add_category_widget::AddCategoryWidget;
pub use self::add_feed_widget::AddCategory;
use self::add_feed_widget::AddFeedWidget;
use self::add_tag_widget::AddTagWidget;

use crate::app::App;
use glib::clone;
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, EventControllerKey, Inhibit, ListBox, ListBoxRow, Popover,
    Stack, Widget,
};
use news_flash::models::{PluginCapabilities, Url};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/add_popover.ui")]
    pub struct AddPopover {
        #[template_child]
        pub main_stack: TemplateChild<Stack>,
        #[template_child]
        pub select_list_box: TemplateChild<ListBox>,
        #[template_child]
        pub feed_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub category_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub tag_row: TemplateChild<ListBoxRow>,
        #[template_child]
        pub key_controller: TemplateChild<EventControllerKey>,

        #[template_child]
        pub add_tag_widget: TemplateChild<AddTagWidget>,
        #[template_child]
        pub add_category_widget: TemplateChild<AddCategoryWidget>,
        #[template_child]
        pub add_feed_widget: TemplateChild<AddFeedWidget>,
    }

    impl Default for AddPopover {
        fn default() -> Self {
            Self {
                main_stack: TemplateChild::default(),
                select_list_box: TemplateChild::default(),
                feed_row: TemplateChild::default(),
                category_row: TemplateChild::default(),
                tag_row: TemplateChild::default(),
                key_controller: TemplateChild::default(),

                add_tag_widget: TemplateChild::default(),
                add_category_widget: TemplateChild::default(),
                add_feed_widget: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AddPopover {
        const NAME: &'static str = "AddPopover";
        type ParentType = Popover;
        type Type = super::AddPopover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AddPopover {
        fn constructed(&self, pop: &Self::Type) {
            pop.init();
        }
    }

    impl WidgetImpl for AddPopover {}

    impl PopoverImpl for AddPopover {}
}

glib::wrapper! {
    pub struct AddPopover(ObjectSubclass<imp::AddPopover>)
        @extends Widget, Popover;
}

impl AddPopover {
    pub fn new_for_feed_url(feed_url: &Url) -> Self {
        let dialog = Self::new();
        let imp = imp::AddPopover::from_instance(&dialog);
        imp.main_stack.set_visible_child_name("feed_page");
        imp.add_feed_widget.parse_feed_url(&feed_url);
        dialog
    }

    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    fn init(&self) {
        let imp = imp::AddPopover::from_instance(self);

        imp.key_controller.connect_key_pressed(
            clone!(@weak self as popover => @default-panic, move |_controller, key, _keycode, _state| {
                if key == gdk4::keys::constants::Escape {
                    popover.popdown();
                    Inhibit(true)
                } else {
                    Inhibit(false)
                }
            }),
        );

        imp.add_tag_widget.connect_local(
            "tag-added",
            false,
            clone!(@weak self as popover => @default-panic, move |_| {
                popover.popdown();
                None
            }),
        );

        imp.add_category_widget.connect_local(
            "category-added",
            false,
            clone!(@weak self as popover => @default-panic, move |_| {
                popover.popdown();
                None
            }),
        );

        imp.add_feed_widget.connect_local(
            "feed-added",
            false,
            clone!(@weak self as popover => @default-panic, move |_| {
                popover.popdown();
                None
            }),
        );

        self.parse_features();

        imp.select_list_box
            .connect_row_activated(clone!(@weak self as this => @default-panic, move |_list, row| {
                let imp = imp::AddPopover::from_instance(&this);
                let index = row.index();

                if index == 0 {
                    // Feed
                    imp.main_stack.set_visible_child_name("feed_page");
                } else if index == 1 {
                    // Category
                    imp.main_stack.set_visible_child_name("category_page");
                } else if index == 2 {
                    // Tag
                    imp.main_stack.set_visible_child_name("tag_page");
                }
            }));

        imp.main_stack.set_visible_child_name("start");

        // reset on close
        self.connect_closed(|popover| {
            let imp = imp::AddPopover::from_instance(&popover);

            imp.main_stack.set_visible_child_name("start");
            imp.add_tag_widget.reset();
            imp.add_category_widget.reset();
            imp.add_feed_widget.reset();
        });
    }

    pub fn parse_features(&self) {
        let imp = imp::AddPopover::from_instance(self);

        let features = App::default().features();
        imp.feed_row
            .set_sensitive(features.read().contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        imp.category_row
            .set_sensitive(features.read().contains(PluginCapabilities::MODIFY_CATEGORIES));
        imp.tag_row
            .set_sensitive(features.read().contains(PluginCapabilities::SUPPORT_TAGS));
    }
}
