use gio::SimpleAction;
use std::path::PathBuf;
use std::sync::Arc;
use std::time;

use crate::content_page::ContentPageState;
use crate::error_dialog::ErrorDialog;
use crate::i18n::{i18n, i18n_f};
use crate::sidebar::FeedListItemID;
use ashpd::{desktop::open_uri, WindowIdentifier};
use futures::channel::oneshot::{self, Sender as OneShotSender};
use futures::executor::{ThreadPool, ThreadPoolBuilder};
use futures::FutureExt;
use gdk4::prelude::*;
use gio::{subclass::prelude::*, ApplicationFlags, Notification, NotificationPriority, ThemedIcon};
use glib::{clone, subclass::types::ObjectSubclass, Continue, Receiver, Sender, SourceId};
use gtk4::builders::FileChooserNativeBuilder;
use gtk4::prelude::*;
use gtk4::{subclass::prelude::GtkApplicationImpl, FileChooserAction, FileFilter, ResponseType};
use lazy_static::lazy_static;
use libadwaita::subclass::prelude::*;
use log::{error, info, warn};
use news_flash::models::{
    ArticleID, Category, CategoryID, FatArticle, FavIcon, Feed, FeedID, LoginData, PasswordLogin, PluginCapabilities,
    PluginID, Tag, TagID, Thumbnail, Url,
};
use news_flash::{NewsFlash, NewsFlashError, NewsFlashErrorKind};
use parking_lot::RwLock;
use tokio::runtime::Runtime;
use tokio::time::{error::Elapsed, timeout, Duration};

use crate::about_dialog::NewsFlashAbout;
use crate::add_popover::AddCategory;
use crate::article_list::{MarkUpdate, ReadUpdate};
use crate::article_view::ArticleView;
use crate::config::{APP_ID, VERSION};
use crate::discover::DiscoverDialog;
use crate::login_screen::{PasswordLoginPrevPage, WebLoginPrevPage};
use crate::main_window::MainWindow;
use crate::rename_dialog::RenameDialog;
use crate::settings::{Settings, SettingsDialog, UserDataSize};
use crate::shortcuts_dialog::ShortcutsDialog;
use crate::sidebar::{models::SidebarSelection, FeedListDndAction};
use crate::undo_action::UndoAction;
use crate::util::{DesktopSettings, GtkUtil, MercuryParser, Util, UtilError, CHANNEL_ERROR, RUNTIME_ERROR};

lazy_static! {
    pub static ref CONFIG_DIR: PathBuf = glib::user_config_dir().join("news-flash");
    pub static ref DATA_DIR: PathBuf = glib::user_data_dir().join("news-flash");
    pub static ref WEBKIT_DATA_DIR: PathBuf = DATA_DIR.join("Webkit");
}

#[derive(Debug, Clone)]
pub struct NotificationCounts {
    pub new: i64,
    pub unread: i64,
}

#[derive(Debug)]
pub enum Action {
    ErrorSimpleMessage(String),
    Error(String, NewsFlashError),
    LoadFavIcon((FeedID, OneShotSender<Option<FavIcon>>)),
    LoadThumbnail((ArticleID, OneShotSender<Option<Thumbnail>>)),
    ShowWelcomePage,
    ShowContentPage,
    ShowPasswordLogin(PluginID, Option<PasswordLogin>, PasswordLoginPrevPage),
    ShowOauthLogin(PluginID, WebLoginPrevPage),
    ShowResetPage,
    ShowDiscoverDialog,
    ShowSettingsWindow,
    ShowShortcutWindow,
    ShowAboutWindow,
    CancelReset,
    UpdateLogin,
    Login(LoginData),
    ResetAccount,
    ResetAccountError(NewsFlashError),
    ScheduleSync,
    Sync,
    InitSync,
    MarkArticleRead(ReadUpdate),
    MarkArticle(MarkUpdate),
    ToggleArticleRead,
    ToggleArticleMarked,
    UpdateSidebar,
    UpdateArticleList,
    LoadMoreArticles,
    SidebarSelection(SidebarSelection),
    SelectNextArticle,
    SelectPrevArticle,
    UpdateArticleHeader,
    ShowArticle(Arc<ArticleID>),
    RedrawArticle,
    SearchTerm(String),
    SetSidebarRead,
    AddFeed((Url, Option<String>, Option<AddCategory>)),
    AddCategory(String),
    AddTag(String, String),
    DeleteSidebarSelection,
    DeleteFeed(FeedID),
    DeleteCategory(CategoryID),
    DeleteTag(TagID),
    TagArticle(ArticleID, TagID),
    UntagArticle(ArticleID, TagID),
    DragAndDrop(FeedListDndAction),
    StartGrabArticleContent,
    MercuryGrabArticleContent(FatArticle, Option<Url>, Vec<Url>),
    FinishGrabArticleContent(ArticleID, Option<FatArticle>, bool),
    QueueQuit,
    SetOfflineMode(bool),
    IgnoreTLSErrors,
    OpenSelectedArticle,
    OpenUrlInDefaultBrowser(String),
    QueryDiskSpace(OneShotSender<UserDataSize>),
    ClearCache(OneShotSender<()>),
}

pub struct AppPrivate {
    pub window: RwLock<Option<Arc<MainWindow>>>,
    pub sender: Sender<Action>,
    pub receiver: RwLock<Option<Receiver<Action>>>,
    pub news_flash: Arc<RwLock<Option<NewsFlash>>>,
    pub news_flash_error: Arc<RwLock<Option<NewsFlashError>>>,
    pub settings: Arc<RwLock<Settings>>,
    pub sync_source_id: RwLock<Option<SourceId>>,
    pub threadpool: ThreadPool,
    pub icon_threadpool: ThreadPool,
    pub shutdown_in_progress: Arc<RwLock<bool>>,
    pub start_headless: Arc<RwLock<bool>>,
    pub features: Arc<RwLock<PluginCapabilities>>,
    pub desktop_settings: DesktopSettings,
}

#[glib::object_subclass]
impl ObjectSubclass for AppPrivate {
    const NAME: &'static str = "AppPrivate";
    type Type = App;
    type ParentType = libadwaita::Application;

    fn new() -> Self {
        info!("NewsFlash {} ({})", VERSION, APP_ID);

        let shutdown_in_progress = Arc::new(RwLock::new(false));
        let start_headless = Arc::new(RwLock::new(false));
        let (sender, r) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
        let receiver = RwLock::new(Some(r));

        let cpu_cores = num_cpus::get();
        let max_size = 64;
        let min_size = 8;
        let pool_size = if cpu_cores > max_size {
            max_size
        } else if cpu_cores < min_size {
            min_size
        } else {
            cpu_cores
        };
        let threadpool = ThreadPoolBuilder::new()
            .pool_size(pool_size)
            .create()
            .expect("Failed to init thread pool");
        let icon_threadpool = ThreadPoolBuilder::new()
            .pool_size(16)
            .create()
            .expect("Failed to init thread pool");

        let news_flash = Arc::new(RwLock::new(None));
        let features = Arc::new(RwLock::new(PluginCapabilities::NONE));
        let settings = Settings::open().expect("Failed to access settings file");
        let settings = Arc::new(RwLock::new(settings));
        let desktop_settings = DesktopSettings::default();

        let app = Self {
            window: RwLock::new(None),
            sender: sender.clone(),
            receiver,
            news_flash,
            news_flash_error: Arc::new(RwLock::new(None)),
            settings,
            sync_source_id: RwLock::new(None),
            threadpool,
            icon_threadpool,
            shutdown_in_progress,
            start_headless,
            features,
            desktop_settings,
        };

        if let Ok(news_flash_lib) = NewsFlash::try_load(&DATA_DIR, &CONFIG_DIR) {
            info!("Successful load from config");

            if let Ok(features) = news_flash_lib.features() {
                *app.features.write() = features;
            }
            app.news_flash.write().replace(news_flash_lib);
            if app.settings.read().get_sync_on_startup() {
                Util::send_static(&sender, Action::Sync);
            }
            Util::send_static(&sender, Action::ScheduleSync);
        } else {
            warn!("No account configured");
        }

        app
    }
}

impl ObjectImpl for AppPrivate {}

impl GtkApplicationImpl for AppPrivate {}

impl ApplicationImpl for AppPrivate {
    fn startup(&self, app: &App) {
        // Workaround to still load style.css if app-id has '.Devel' suffix
        let devel_suffix = ".Devel";
        if APP_ID.ends_with(devel_suffix) {
            if let Some(id_str) = APP_ID.strip_suffix(devel_suffix) {
                let new_id = format!("/{}/", id_str.replace('.', "/"));
                app.set_resource_base_path(Some(&new_id));
            }
        }

        GtkUtil::register_symbolic_icons();
        GtkUtil::register_ui_templates();
        GtkUtil::register_styles();

        self.parent_startup(app);
    }

    fn activate(&self, app: &App) {
        if let Some(window) = self.window.read().as_ref() {
            window.show();
            window.present();

            return;
        }

        self.desktop_settings.init();

        let window = MainWindow::new();
        self.window.write().replace(Arc::new(window));
        app.add_window(&*self.get_main_window());
        self.get_main_window().init(&self.shutdown_in_progress);

        app.setup_actions();

        if *self.start_headless.read() {
            self.get_main_window().hide();
        } else {
            self.get_main_window().present();
        }

        let receiver = self.receiver.write().take().expect(CHANNEL_ERROR);
        receiver.attach(None, clone!(@strong app => move |action| app.process_action(action)));
    }
}

impl AdwApplicationImpl for AppPrivate {}

impl AppPrivate {
    fn get_main_window(&self) -> Arc<MainWindow> {
        self.window.read().clone().expect("window not initialized")
    }

    fn login(&self, data: LoginData) {
        let id = match &data {
            LoginData::OAuth(oauth) => oauth.id.clone(),
            LoginData::Password(pass) => pass.id.clone(),
            LoginData::None(id) => id.clone(),
        };
        let news_flash_lib = match NewsFlash::new(&DATA_DIR, &CONFIG_DIR, &id) {
            Ok(news_flash) => news_flash,
            Err(error) => {
                match &data {
                    LoginData::OAuth(_) => self.get_main_window().login_page().web_login().show_error(error),
                    LoginData::Password(_) => self.get_main_window().login_page().password_login().show_error(error),
                    LoginData::None(_) => {}
                }
                return;
            }
        };

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let data_clone = data.clone();
        let app_features = self.features.clone();
        let thread_future = async move {
            let result = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(news_flash_lib.login(data_clone, &Util::build_client()));
            match result {
                Ok(()) => {
                    // query features
                    let features = news_flash_lib.features();
                    let features = match features {
                        Ok(features) => features,
                        Err(error) => {
                            sender.send(Err(error)).expect(CHANNEL_ERROR);
                            return;
                        }
                    };
                    *app_features.write() = features;
                    // create main obj
                    news_flash.write().replace(news_flash_lib);
                    // show content page
                    Util::send(Action::ShowContentPage);
                    // schedule initial sync
                    Util::send(Action::InitSync);
                    Util::send(Action::ScheduleSync);

                    sender.send(Ok(())).expect(CHANNEL_ERROR);
                }
                Err(error) => {
                    error!("Login failed! Plugin: {}, Error: {}", id, error);
                    sender.send(Err(error)).expect(CHANNEL_ERROR);
                }
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @weak main_window => @default-panic, move |res|
        {
            match res {
                Ok(Err(error)) => {
                    match data {
                        LoginData::OAuth(_) => {
                            main_window.login_page().web_login().show_error(error);
                        }
                        LoginData::Password(_) => {
                            main_window.login_page().password_login().show_error(error);
                        }
                        LoginData::None(_) => {
                            // NOTHING
                        }
                    }
                },
                Ok(Ok(())) => {
                    main_window.update_features();
                }
                _ => {}
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn update_login(&self) {
        self.get_main_window().content_page().dismiss_notifications();
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Some(login_data) = news_flash.get_login_data() {
                match login_data {
                    LoginData::None(_id) => error!("updating login for local should never happen!"),
                    LoginData::Password(password_data) => {
                        let plugin_id = password_data.id.clone();
                        Util::send(Action::ShowPasswordLogin(
                            plugin_id.clone(),
                            Some(password_data),
                            PasswordLoginPrevPage::Content(plugin_id),
                        ));
                    }
                    LoginData::OAuth(oauth_data) => {
                        let plugin_id = oauth_data.id.clone();
                        Util::send(Action::ShowOauthLogin(
                            plugin_id.clone(),
                            WebLoginPrevPage::Content(plugin_id),
                        ));
                    }
                }
            }
        }
    }

    fn reset_account(&self) {
        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let news_flash = self.news_flash.clone();
        let thread_future = async move {
            if let Some(news_flash) = news_flash.read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.logout(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let main_window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @weak main_window,
            @weak self.news_flash as news_flash,
            @weak self.features as features => @default-panic, move |res| match res
        {
            Ok(Ok(())) => {
                news_flash.write().take();
                main_window.content_page().clear();
                main_window.content_page().articleview_column().show_article(None, None);
                Util::send(Action::ShowWelcomePage);
            }
            Ok(Err(error)) => {
                Util::send(Action::ResetAccountError(error));
            }
            Err(error) => {
                log::error!("Unexpected error completing account reset: {}", error);
                Util::send(Action::ResetAccountError(NewsFlashErrorKind::Unknown.into()));
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn schedule_sync(&self) {
        GtkUtil::remove_source(self.sync_source_id.write().take());
        let sync_interval = self.settings.read().get_sync_interval();
        if let Some(sync_interval) = sync_interval.to_seconds() {
            self.sync_source_id
                .write()
                .replace(glib::timeout_add_seconds_local(sync_interval, || {
                    Util::send(Action::Sync);
                    Continue(true)
                }));
        } else {
            self.sync_source_id.write().take();
        }
    }

    fn load_favicon(&self, feed_id: FeedID, oneshot_sender: OneShotSender<Option<FavIcon>>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let favicon = match Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.get_icon_info(&feed_id, &Util::build_client()))
                {
                    Ok(favicon) => Some(favicon),
                    Err(_) => {
                        warn!("Failed to load favicon for feed: '{}'", feed_id);
                        None
                    }
                };
                oneshot_sender.send(favicon).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };

        self.icon_threadpool.spawn_ok(thread_future);
    }

    fn load_thumbnail(&self, article_id: ArticleID, oneshot_sender: OneShotSender<Option<Thumbnail>>) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let thumbnail = match Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.get_article_thumbnail(&article_id, &Util::build_client()))
                {
                    Ok(thumbnail) => Some(thumbnail),
                    Err(_) => {
                        log::debug!("Failed to load thumbnail for article: '{}'", article_id);
                        None
                    }
                };
                oneshot_sender.send(thumbnail).expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };

        self.icon_threadpool.spawn_ok(thread_future);
    }

    fn mark_article_read(&self, update: ReadUpdate) {
        self.get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, Some(update.read), None);

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let article_id_vec = vec![(*update.article_id).clone()];
        let read_status = update.read;
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_read(&article_id_vec, read_status, &Util::build_client())),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };

        let window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @strong self.news_flash as news_flash,
            @strong self.features as features,
            @weak window => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to mark article read: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(Action::Error(message, error));
                    Util::send(Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::ErrorSimpleMessage(message));
                    Util::send(Action::UpdateArticleList);
                }
            };

            Util::send(Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = window.content_page().articleview_column().article_view().get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == *update.article_id {
                    visible_article.unread = update.read;
                    window.content_page()
                        .articleview_column()
                        .show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                    window.content_page()
                        .articleview_column()
                        .article_view()
                        .update_visible_article(Some(visible_article.unread), None);
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn mark_article(&self, update: MarkUpdate) {
        self.get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .set_article_row_state(&update.article_id, None, Some(update.marked));

        let (sender, receiver) = oneshot::channel::<Result<(), NewsFlashError>>();

        let article_id_vec = vec![(*update.article_id).clone()];
        let mark_status = update.marked;
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                sender
                    .send(
                        Runtime::new()
                            .expect(RUNTIME_ERROR)
                            .block_on(news_flash.set_article_marked(
                                &article_id_vec,
                                mark_status,
                                &Util::build_client(),
                            )),
                    )
                    .expect(CHANNEL_ERROR);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };

        let window = self.get_main_window();
        let glib_future = receiver.map(clone!(
            @weak window => @default-panic, move |res|
        {
            match res {
                Ok(Ok(())) => {}
                Ok(Err(error)) => {
                    let message = format!("Failed to star article: '{}'", update.article_id);
                    error!("{}", message);
                    Util::send(Action::Error(message, error));
                    Util::send(Action::UpdateArticleList);
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::ErrorSimpleMessage(message));
                    Util::send(Action::UpdateArticleList);
                }
            };

            Util::send(Action::UpdateSidebar);
            let (visible_article, visible_article_enclosures) = window.content_page().articleview_column().article_view().get_visible_article();
            if let Some(mut visible_article) = visible_article {
                if visible_article.article_id == *update.article_id {
                    visible_article.marked = update.marked;
                    window.content_page()
                        .articleview_column()
                        .show_article(Some(&visible_article), visible_article_enclosures.as_ref());
                    window.content_page()
                        .articleview_column()
                        .article_view()
                        .update_visible_article(None, Some(visible_article.marked));
                }
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn toggle_selected_article_read(&self) {
        // get selected article from list
        let selected_article = self
            .get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(ReadUpdate {
                article_id: selected_article.article_id(),
                read: selected_article.read().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .get_main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| ReadUpdate {
                article_id: Arc::new(article.article_id.clone()),
                read: article.unread.invert(),
            })
        };

        if let Some(update) = update {
            self.mark_article_read(update);
        }
    }

    fn toggle_article_read(&self, article_id: &Arc<ArticleID>) {
        if let Some(article_model) = self
            .get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = ReadUpdate {
                article_id: article_id.clone(),
                read: article_model.read().invert().into(),
            };

            self.mark_article_read(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    fn toggle_selected_article_marked(&self) {
        // get selected article from list
        let selected_article = self
            .get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_selected_article_model();

        let update = if let Some(selected_article) = selected_article {
            Some(MarkUpdate {
                article_id: selected_article.article_id(),
                marked: selected_article.marked().invert().into(),
            })
        } else {
            // if no selected article in list check article view next
            let (article, _enclosure) = self
                .get_main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .get_visible_article();

            article.map(|article| MarkUpdate {
                article_id: Arc::new(article.article_id.clone()),
                marked: article.marked.invert(),
            })
        };

        if let Some(update) = update {
            Util::send(Action::MarkArticle(update));
        }
    }

    fn toggle_article_marked(&self, article_id: &Arc<ArticleID>) {
        if let Some(article_model) = self
            .get_main_window()
            .content_page()
            .article_list_column()
            .article_list()
            .get_model(article_id)
        {
            let update = MarkUpdate {
                article_id: article_id.clone(),
                marked: article_model.marked().invert().into(),
            };

            self.mark_article(update);
        } else {
            log::warn!("article '{:?}' not part of article-list", article_id);
        }
    }

    fn spawn_shortcut_window(&self) {
        let dialog = ShortcutsDialog::new(&*self.get_main_window(), &*self.settings.read());
        dialog.widget.present();
    }

    fn spawn_about_window(&self) {
        NewsFlashAbout::show(&*self.get_main_window());
    }

    fn spawn_settings_window(&self) {
        let dialog = SettingsDialog::new();
        dialog.init();
        dialog.set_transient_for(Some(&*self.get_main_window()));
        dialog.present();
    }

    fn spawn_discover_dialog(&self) {
        let dialog = DiscoverDialog::new(&*self.get_main_window());
        dialog.present();
    }

    fn add_feed(&self, feed_url: Url, title: Option<String>, category: Option<AddCategory>) {
        info!("add feed '{}'", feed_url);

        let thread_future = async move {
            let error_message = "Failed to add feed".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let category_id = match category {
                    Some(category) => match category {
                        AddCategory::New(category_title) => {
                            let client = Util::build_client();
                            let add_category_future = news_flash.add_category(&category_title, None, None, &client);
                            let category = match Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future) {
                                Ok(category) => category,
                                Err(error) => {
                                    error!("{}: Can't add Category", error_message);
                                    Util::send(Action::Error(error_message, error));
                                    return;
                                }
                            };
                            Some(category.category_id)
                        }
                        AddCategory::Existing(category_id) => Some(category_id),
                    },
                    None => None,
                };

                let client = Util::build_client();
                let add_feed_future = news_flash
                    .add_feed(&feed_url, title, category_id, &client)
                    .map(|result| match result {
                        Ok(_) => {}
                        Err(error) => {
                            error!("{}: Can't add Feed", error_message);
                            Util::send(Action::Error(error_message.clone(), error));
                        }
                    });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_feed_future);
                Util::send(Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn add_category(&self, title: String) {
        info!("add category '{}'", title);

        let thread_future = async move {
            let error_message = "Failed to add category".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let client = Util::build_client();
                let add_category_future =
                    news_flash
                        .add_category(&title, None, None, &client)
                        .map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                error!("{}: Can't add Category", error_message);
                                Util::send(Action::Error(error_message.clone(), error));
                            }
                        });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_category_future);
                Util::send(Action::UpdateSidebar);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn add_tag(&self, color: String, title: String) {
        info!("add tag '{}'", title);

        let thread_future = async move {
            let error_message = "Failed to add tag".to_owned();
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let client = Util::build_client();
                let add_tag_future =
                    news_flash
                        .add_tag(&title, Some(color), None, &client)
                        .map(|result| match result {
                            Ok(_) => {}
                            Err(error) => {
                                error!("{}: Can't add Tag", error_message);
                                Util::send(Action::Error(error_message.clone(), error));
                            }
                        });
                Runtime::new().expect(RUNTIME_ERROR).block_on(add_tag_future);
                Util::send(Action::UpdateSidebar);
                Util::send(Action::UpdateArticleHeader);
            } else {
                let message = "Failed to lock NewsFlash.".to_owned();
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
            }
        };
        self.threadpool.spawn_ok(thread_future);
    }

    fn rename_feed(&self, feed: &Feed, new_title: &str) {
        let feed = feed.clone();
        let new_title = new_title.to_string();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.rename_feed(
                    &feed,
                    &new_title,
                    &Util::build_client(),
                )) {
                    Util::send(Action::Error("Failed to rename feed.".to_owned(), error));
                }
            }

            Util::send(Action::UpdateArticleList);
            Util::send(Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn rename_category(&self, category: Category, new_title: String) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.rename_category(&category, &new_title, &Util::build_client()))
                {
                    Util::send(Action::Error("Failed to rename category.".to_owned(), error));
                }
            }

            Util::send(Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn rename_tag(&self, tag: &Tag, new_title: &str) {
        let tag = tag.clone();
        let new_title = new_title.to_string();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.rename_tag(
                    &tag,
                    &new_title,
                    &Util::build_client(),
                )) {
                    Util::send(Action::Error("Failed to rename feed.".to_owned(), error));
                }
            }

            Util::send(Action::UpdateArticleList);
            Util::send(Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_selection(&self) {
        let selection = self
            .get_main_window()
            .content_page()
            .sidebar_column()
            .sidebar()
            .get_selection();
        let undo_action = match selection {
            SidebarSelection::All => {
                warn!("Trying to delete item while 'All Articles' is selected");
                None
            }
            SidebarSelection::FeedList(id, label) => match &*id {
                FeedListItemID::Category(category_id) => Some(UndoAction::DeleteCategory(category_id.clone(), label)),
                FeedListItemID::Feed(feed_id, _parent_id) => Some(UndoAction::DeleteFeed(feed_id.clone(), label)),
            },
            SidebarSelection::Tag(tag_id, label) => Some(UndoAction::DeleteTag((*tag_id).clone(), label)),
        };
        if let Some(undo_action) = undo_action {
            self.get_main_window().show_undo_bar(undo_action);
        }
    }

    fn delete_feed(&self, feed_id: FeedID) {
        let processing_actions = self.get_main_window().content_page().processing_undo_actions();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let (feeds, _mappings) = match news_flash.get_feeds() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete feed.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoAction::DeleteFeed(feed_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(feed) = feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                    info!("delete feed '{}' (id: {})", feed.label, feed.feed_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_feed(&feed, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete feed.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete feed: feed with id '{}' not found.", feed_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    error!("feed not found: {}", feed_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoAction::DeleteFeed(feed_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_category(&self, category_id: CategoryID) {
        let processing_actions = self.get_main_window().content_page().processing_undo_actions();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let categories = match news_flash.get_categories() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete category.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoAction::DeleteCategory(category_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(category) = categories.iter().find(|c| c.category_id == category_id).cloned() {
                    info!("delete category '{}' (id: {})", category.label, category.category_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_category(&category, true, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete category.".to_owned(), error));
                    }
                } else {
                    let message = format!(
                        "Failed to delete category: category with id '{}' not found.",
                        category_id
                    );
                    Util::send(Action::ErrorSimpleMessage(message));
                    error!("category not found: {}", category_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoAction::DeleteCategory(category_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn delete_tag(&self, tag_id: TagID) {
        let processing_actions = self.get_main_window().content_page().processing_undo_actions();
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to delete tag.".to_owned(), error));
                        processing_actions
                            .write()
                            .remove(&UndoAction::DeleteTag(tag_id.clone(), "".into()));
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("delete tag '{}' (id: {})", tag.label, tag.tag_id);
                    if let Err(error) = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(news_flash.remove_tag(&tag, &Util::build_client()))
                    {
                        Util::send(Action::Error("Failed to delete tag.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to delete tag: tag with id '{}' not found.", tag_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
            processing_actions
                .write()
                .remove(&UndoAction::DeleteTag(tag_id.clone(), "".into()));
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn tag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to tag article.".to_owned(), error));
                        return;
                    }
                };
                let article = match news_flash.get_article(&article_id) {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error(
                            "Failed to tag article. Article not found.".to_owned(),
                            error,
                        ));
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("tag article '{}' with '{}'", article_id, tag.tag_id);
                    if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.tag_article(
                        &article,
                        &tag,
                        &Util::build_client(),
                    )) {
                        Util::send(Action::Error("Failed to tag article.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to tag article: tag with id '{}' not found.", tag_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn untag_article(&self, article_id: ArticleID, tag_id: TagID) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let tags = match news_flash.get_tags() {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error("Failed to untag article.".to_owned(), error));
                        return;
                    }
                };
                let article = match news_flash.get_article(&article_id) {
                    Ok(res) => res,
                    Err(error) => {
                        Util::send(Action::Error(
                            "Failed to untag article. Article not found.".to_owned(),
                            error,
                        ));
                        return;
                    }
                };

                if let Some(tag) = tags.iter().find(|t| t.tag_id == tag_id).cloned() {
                    info!("untag article '{}' with '{}'", article_id, tag.tag_id);
                    if let Err(error) = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.untag_article(
                        &article,
                        &tag,
                        &Util::build_client(),
                    )) {
                        Util::send(Action::Error("Failed to untag article.".to_owned(), error));
                    }
                } else {
                    let message = format!("Failed to tag article: untag with id '{}' not found.", tag_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    error!("tag not found: {}", tag_id);
                }
            }
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn drag_and_drop(&self, action: FeedListDndAction) {
        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let runtime = Runtime::new().expect(RUNTIME_ERROR);

                match &*action.item_id {
                    FeedListItemID::Category(category_id) => {
                        if let Err(error) = runtime.block_on(news_flash.move_category(
                            category_id,
                            &action.new_parent,
                            &Util::build_client(),
                        )) {
                            Util::send(Action::Error("Failed to move category.".to_owned(), error));
                        }
                    }
                    FeedListItemID::Feed(feed_id, old_parent) => {
                        if let Err(error) = runtime.block_on(news_flash.move_feed(
                            feed_id,
                            old_parent,
                            &action.new_parent,
                            &Util::build_client(),
                        )) {
                            Util::send(Action::Error("Failed to move feed.".to_owned(), error));
                        }
                    }
                }
            }
            Util::send(Action::UpdateSidebar);
        };

        self.threadpool.spawn_ok(thread_future);
    }

    fn export_article(&self) {
        if let (Some(article), _article_enclosures) = self
            .get_main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            let news_flash = self.news_flash.clone();
            let window_state = self.get_main_window().content_page().state().clone();
            let settings = self.settings.clone();
            let threadpool = self.threadpool.clone();
            let main_window = self.get_main_window().clone();

            let future = async move {
                let dialog = FileChooserNativeBuilder::new()
                    .transient_for(&*main_window)
                    .accept_label(&i18n("_Save"))
                    .cancel_label(&i18n("_Cancel"))
                    .title(&i18n("Export Article"))
                    .action(FileChooserAction::Save)
                    .modal(true)
                    .build();

                let filter = FileFilter::new();
                filter.add_pattern("*.html");
                filter.add_mime_type("text/html");
                filter.set_name(Some("HTML"));
                dialog.add_filter(&filter);
                dialog.set_filter(&filter);
                if let Some(title) = &article.title {
                    dialog.set_current_name(&format!("{}.html", title.replace("/", "_")));
                } else {
                    dialog.set_current_name("Article.html");
                }
                dialog
                    .set_current_folder(&gio::File::for_path(glib::home_dir()))
                    .expect("FIXME");

                let (tx, rx) = oneshot::channel::<Option<gio::File>>();
                let tx = RwLock::new(Some(tx));

                dialog.connect_response(move |dialog, response| {
                    if response == ResponseType::Accept {
                        match dialog.file() {
                            Some(file) => {
                                if let Some(tx) = tx.write().take() {
                                    tx.send(Some(file)).unwrap();
                                }
                            }
                            None => {
                                Util::send(Action::ErrorSimpleMessage("No file set.".to_owned()));
                                return;
                            }
                        }
                    }
                });

                dialog.show();

                let file = if let Ok(Some(file)) = rx.await {
                    file
                } else {
                    return;
                };

                main_window
                    .content_page()
                    .articleview_column()
                    .start_more_actions_spinner();

                let prefer_dark_theme = libadwaita::StyleManager::default()
                    .map(|sm| sm.is_dark())
                    .unwrap_or(false);
                let prefer_scraped_content = App::default().content_page_state().read().get_prefer_scraped_content();

                let (tx, rx) = oneshot::channel::<()>();
                let tx = RwLock::new(Some(tx));

                let window_state = window_state.clone();
                let article = article.clone();
                let thread_future = async move {
                    let tx = tx.write().take();
                    if let Some(news_flash) = news_flash.read().as_ref() {
                        let article = if window_state.read().get_offline() {
                            article
                        } else {
                            match Runtime::new().expect(RUNTIME_ERROR).block_on(
                                news_flash.article_download_images(&article.article_id, &Util::build_client()),
                            ) {
                                Ok(article) => article,
                                Err(error) => {
                                    Util::send(Action::Error("Failed to downlaod article images.".to_owned(), error));
                                    tx.map(|tx| tx.send(()).unwrap());
                                    return;
                                }
                            }
                        };

                        let (feeds, _) = match news_flash.get_feeds() {
                            Ok(feeds) => feeds,
                            Err(error) => {
                                Util::send(Action::Error("Failed to load feeds from db.".to_owned(), error));
                                tx.map(|tx| tx.send(()).unwrap());
                                return;
                            }
                        };
                        let feed = match feeds.iter().find(|&f| f.feed_id == article.feed_id) {
                            Some(feed) => feed,
                            None => {
                                Util::send(Action::ErrorSimpleMessage("Failed to find specific feed.".to_owned()));
                                tx.map(|tx| tx.send(()).unwrap());
                                return;
                            }
                        };
                        let html = ArticleView::build_article_static(
                            "article",
                            &article,
                            &feed.label,
                            Some(&settings),
                            None,
                            None,
                            Some(true),
                            prefer_scraped_content,
                            prefer_dark_theme,
                        );
                        if let Err(error) = GtkUtil::write_bytes_to_file(&html.as_bytes(), &file) {
                            Util::send(Action::ErrorSimpleMessage(error.to_string()));
                        }
                        tx.map(|tx| tx.send(()).unwrap());
                    }
                };

                threadpool.spawn_ok(thread_future);

                let _ = rx.await;
                main_window
                    .content_page()
                    .articleview_column()
                    .stop_more_actions_spinner();
            };

            Util::glib_spawn_future(future);
        }
    }

    fn start_grab_article_content(&self) {
        let (sender, receiver) = oneshot::channel::<Result<Result<FatArticle, NewsFlashError>, Elapsed>>();

        if let (Some(article), _article_enclosures) = self
            .get_main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
        {
            // Article already scraped: just swap to scraped content
            if article.scraped_content.is_some() {
                Util::send(Action::FinishGrabArticleContent(
                    article.article_id.clone(),
                    Some(article),
                    true,
                ));
                return;
            }

            self.get_main_window()
                .content_page()
                .state()
                .write()
                .started_scraping_article(&article.article_id);
            self.get_main_window()
                .content_page()
                .articleview_column()
                .start_scrap_content_spinner();

            let article_id = article.article_id.clone();
            let thread_future = async move {
                if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                    let client = Util::build_client();
                    let news_flash_future = news_flash.article_scrap_content(&article_id, &client);
                    let article = Runtime::new()
                        .expect(RUNTIME_ERROR)
                        .block_on(async { timeout(Duration::from_secs(60), news_flash_future).await });
                    sender.send(article).expect(CHANNEL_ERROR);
                }
            };

            let glib_future = receiver.map(clone!(
                @strong article as oritinal_article => @default-panic, move |res| match res
            {
                Ok(Ok(Ok(article))) => {
                    Util::send(Action::FinishGrabArticleContent(article.article_id.clone(), Some(article), true));
                }
                Ok(Ok(Err(_error))) => {
                    Util::send(Action::MercuryGrabArticleContent(oritinal_article, None, vec![]));
                }
                Ok(Err(_error)) => {
                    log::warn!("Internal scraper elapsed");
                    Util::send(Action::MercuryGrabArticleContent(oritinal_article, None, vec![]));
                }
                Err(error) => {
                    let message = format!("Sender error: {}", error);
                    error!("{}", message);
                    Util::send(Action::ErrorSimpleMessage(message));
                    Util::send(Action::FinishGrabArticleContent(article.article_id.clone(), None, false));
                }
            }));

            self.threadpool.spawn_ok(thread_future);
            Util::glib_spawn_future(glib_future);
        }
    }

    fn mercury_grab_article_contnet(
        &self,
        article: FatArticle,
        next_page_url: Option<Url>,
        mut previous_urls: Vec<Url>,
    ) {
        let (sender, receiver) = oneshot::channel::<Result<String, UtilError>>();
        let article_url = if let Some(next_page_url) = &next_page_url {
            log::info!("parsing next page: '{}'", next_page_url);
            next_page_url.clone()
        } else {
            article.url.clone().unwrap()
        };
        let thread_future = async move {
            let js_string = Runtime::new()
                .expect(RUNTIME_ERROR)
                .block_on(MercuryParser::prep(&article_url, &Util::build_client()));
            sender.send(js_string).expect(CHANNEL_ERROR);
        };
        let original_article_id = article.article_id.clone();

        let glib_future = receiver.map(clone!(
            @strong article as original_article,
            @strong original_article_id => @default-panic, move |res| match res
        {
            Ok(Ok(js_string)) => {
                match MercuryParser::parse(&js_string) {
                    Ok(mercury_output) => {
                        let mut updated_article = original_article;
                        if let Some(content) = mercury_output.content {
                            if let Some(scraped_content) = updated_article.scraped_content.as_mut() {
                                scraped_content.push_str(&content);
                            } else {
                                updated_article.scraped_content = Some(content);
                            }

                            if let Some(next_page) = mercury_output.next_page_url {
                                if let Ok(next_page) = Url::parse(&next_page) {
                                    if previous_urls.contains(&next_page) {
                                        // we already did parse this url -> probably done
                                        Util::send(Action::FinishGrabArticleContent(original_article_id.clone(), Some(updated_article), true));
                                    } else {
                                        if let Ok(current_url) = Url::parse(&mercury_output.url) {
                                            previous_urls.push(current_url);
                                        }
                                        Util::send(Action::MercuryGrabArticleContent(updated_article, Some(next_page), previous_urls));
                                    }
                                }
                            } else {
                                Util::send(Action::FinishGrabArticleContent(original_article_id.clone(), Some(updated_article), true));
                            }
                        } else {
                            let message = format!("Parsing sucessful, but no content could be extracted");
                            error!("{}", message);
                            Util::send(Action::ErrorSimpleMessage(message));
                            // if this was the first page that failed finish with 'None', pass the article otherwise
                            let article = if next_page_url.is_some() { Some(updated_article) } else { None };
                            Util::send(Action::FinishGrabArticleContent(original_article_id.clone(), article, true));
                        }
                    }
                    Err(error) => {
                        let message = format!("Download of article failed: {}", error);
                        error!("{}", message);
                        Util::send(Action::ErrorSimpleMessage(message));
                        Util::send(Action::FinishGrabArticleContent(original_article_id.clone(), None, false));
                    }
                }
            }
            Ok(Err(error)) => {
                let message = format!("Download of article failed: {}", error);
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
                Util::send(Action::FinishGrabArticleContent(original_article.article_id.clone(), None, false));
            }
            Err(error) => {
                let message = format!("Sender error: {}", error);
                error!("{}", message);
                Util::send(Action::ErrorSimpleMessage(message));
                Util::send(Action::FinishGrabArticleContent(original_article.article_id.clone(), None, false));
            }
        }));

        self.threadpool.spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn finish_grab_article_content(&self, article_id: ArticleID, article: Option<FatArticle>, update_db: bool) {
        self.get_main_window()
            .content_page()
            .articleview_column()
            .stop_scrap_content_spinner();

        if update_db {
            if let Some(news_flash) = self.news_flash.read().as_ref() {
                if let Some(updated_article) = &article {
                    news_flash.update_external_scraped_content(updated_article).unwrap();
                }
            }
        }

        self.get_main_window()
            .content_page()
            .state()
            .write()
            .finished_scraping_article(&article_id);

        let scraped_article_id = article.map(|a| Arc::new(a.article_id));
        let visible_article_id = self
            .get_main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article()
            .0
            .map(|a| a.article_id);

        if let (Some(scraped_article_id), Some(visible_article_id)) = (&scraped_article_id, &visible_article_id) {
            if &**scraped_article_id == visible_article_id {
                self.get_main_window().show_article(scraped_article_id.clone());
            }
        } else if scraped_article_id.is_none() {
            self.get_main_window()
                .content_page()
                .articleview_column()
                .update_scrape_content_button_state(false);
        }
    }

    fn import_opml(&self) {
        let window = self.get_main_window();
        let threadpool = self.threadpool.clone();

        let future = async move {
            let dialog = FileChooserNativeBuilder::new()
                .transient_for(&*window)
                .accept_label(&i18n("_Open"))
                .cancel_label(&i18n("_Cancel"))
                .title(&i18n("Export OPML"))
                .action(FileChooserAction::Open)
                .modal(true)
                .build();

            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);

            let (tx, rx) = oneshot::channel::<Option<String>>();
            let tx = RwLock::new(Some(tx));

            dialog.connect_response(move |dialog, response| {
                if let Some(tx) = tx.write().take() {
                    if response == ResponseType::Accept {
                        if let Some(file) = dialog.file() {
                            let buffer = match GtkUtil::read_bytes_from_file(&file) {
                                Ok(buffer) => buffer,
                                Err(error) => {
                                    Util::send(Action::ErrorSimpleMessage(error.to_string()));
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            let opml_content = match String::from_utf8(buffer) {
                                Ok(string) => string,
                                Err(error) => {
                                    Util::send(Action::ErrorSimpleMessage(format!(
                                        "Failed read OPML string: {}",
                                        error
                                    )));
                                    tx.send(None).unwrap();
                                    return;
                                }
                            };

                            tx.send(Some(opml_content)).unwrap();
                        }
                    }
                }
            });

            dialog.show();

            let opml_content = if let Ok(Some(opml_content)) = rx.await {
                opml_content
            } else {
                return;
            };

            window.content_page().start_sync();

            let (tx, rx) = oneshot::channel::<()>();
            let tx = RwLock::new(Some(tx));

            let thread_future = async move {
                let tx = tx.write().take();

                if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                    let result = Runtime::new().expect(RUNTIME_ERROR).block_on(news_flash.import_opml(
                        &opml_content,
                        false,
                        &Util::build_client(),
                    ));

                    if let Err(error) = result {
                        Util::send(Action::Error("Failed to import OPML.".to_owned(), error));
                    } else {
                        Util::send(Action::UpdateSidebar);
                    }
                }

                tx.map(|tx| tx.send(()).unwrap());
            };
            threadpool.spawn_ok(thread_future);

            let _ = rx.await;
            window.content_page().finish_sync();
        };
        Util::glib_spawn_future(future);
    }

    fn export_opml(&self) {
        let news_flash = self.news_flash.clone();
        let main_window = self.get_main_window().clone();
        let future = async move {
            let dialog = FileChooserNativeBuilder::new()
                .transient_for(&*main_window)
                .accept_label(&i18n("_Save"))
                .cancel_label(&i18n("_Cancel"))
                .title(&i18n("Export OPML"))
                .action(FileChooserAction::Save)
                .modal(true)
                .build();

            let filter = FileFilter::new();
            filter.add_pattern("*.OPML");
            filter.add_pattern("*.opml");
            filter.add_mime_type("application/xml");
            filter.add_mime_type("text/xml");
            filter.add_mime_type("text/x-opml");
            filter.set_name(Some("OPML"));
            dialog.add_filter(&filter);
            dialog.set_filter(&filter);
            dialog.set_current_name("NewsFlash.OPML");
            dialog
                .set_current_folder(&gio::File::for_path(glib::home_dir()))
                .unwrap();

            let (tx, rx) = oneshot::channel::<Option<gio::File>>();
            let tx = RwLock::new(Some(tx));

            dialog.connect_response(move |dialog, response| {
                if response == ResponseType::Accept {
                    match dialog.file() {
                        Some(file) => {
                            if let Some(tx) = tx.write().take() {
                                tx.send(Some(file)).unwrap();
                            }
                        }
                        None => {
                            Util::send(Action::ErrorSimpleMessage("No file set.".to_owned()));
                            return;
                        }
                    }
                }
            });

            dialog.show();

            let file = if let Ok(Some(file)) = rx.await {
                file
            } else {
                return;
            };

            if let Some(news_flash) = news_flash.read().as_ref() {
                let opml = match news_flash.export_opml() {
                    Ok(opml) => opml,
                    Err(error) => {
                        Util::send(Action::Error("Failed to get OPML data.".to_owned(), error));
                        return;
                    }
                };

                // Format XML
                if let Ok(tree) = xmltree::Element::parse(opml.as_bytes()) {
                    let write_config = xmltree::EmitterConfig::new().perform_indent(true);
                    let data = std::rc::Rc::new(std::cell::RefCell::new(Some(Vec::new())));
                    let rc_writer = rc_writer::RcOptionWriter::new(data.clone());
                    if tree.write_with_config(rc_writer, write_config.clone()).is_err() {
                        Util::send(Action::ErrorSimpleMessage(
                            "Failed to write OPML data to disc.".to_owned(),
                        ));
                    }
                    let data = data.borrow_mut().take().expect("FIXME");
                    if let Err(error) = GtkUtil::write_bytes_to_file(&data, &file) {
                        Util::send(Action::ErrorSimpleMessage(error.to_string()));
                    }
                } else {
                    Util::send(Action::ErrorSimpleMessage(
                        "Failed to parse OPML data for formatting.".to_owned(),
                    ));
                }
            }
        };

        Util::glib_spawn_future(future);
    }

    fn queue_quit(&self) {
        *self.shutdown_in_progress.write() = true;
        self.get_main_window().close();
        self.get_main_window().content_page().execute_pending_undoable_action();

        // wait for ongoing sync to finish, but limit waiting to max 3s
        let start_wait_time = time::SystemTime::now();
        let max_wait_time = time::Duration::from_secs(3);

        while Self::is_syncing(&self.news_flash)
            && start_wait_time.elapsed().expect("shutdown timer elapsed error") < max_wait_time
        {
            glib::MainContext::default().iteration(true);
        }

        self.force_quit();
    }

    fn force_quit(&self) {
        info!("Shutdown!");
        self.instance().quit();
    }

    fn is_syncing(news_flash: &Arc<RwLock<Option<NewsFlash>>>) -> bool {
        if let Some(news_flash) = news_flash.read().as_ref() {
            if news_flash.is_sync_ongoing() {
                return true;
            }
        }
        false
    }

    fn set_offline(&self, offline: bool) {
        self.get_main_window().content_page().set_offline(offline);

        if let Some(import_opml_action) = self.get_main_window().lookup_action("import-opml") {
            import_opml_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
        if let Some(discover_action) = self.get_main_window().lookup_action("discover") {
            discover_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
        if let Some(offline_action) = self.get_main_window().lookup_action("go-offline") {
            offline_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(!offline);
        }
    }

    fn ignore_tls_errors(&self) {
        if self.settings.write().set_accept_invalid_certs(true).is_err() {
            Util::send(Action::ErrorSimpleMessage("Error writing settings.".to_owned()));
        }
        if self.settings.write().set_accept_invalid_hostnames(true).is_err() {
            Util::send(Action::ErrorSimpleMessage("Error writing settings.".to_owned()));
        }
    }

    fn open_selected_article_in_browser(&self) {
        let article_model = self
            .get_main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .get_visible_article();
        if let (Some(article_model), _article_enclosures) = article_model {
            if let Some(url) = article_model.url {
                Util::send(Action::OpenUrlInDefaultBrowser(url.to_string()));
            } else {
                warn!("Open selected article in browser: No url available.")
            }
        } else {
            warn!("Open selected article in browser: No article Selected.")
        }
    }

    fn open_article_in_browser(&self, article_id: &ArticleID) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Ok(article) = news_flash.get_article(article_id) {
                if let Some(url) = &article.url {
                    self.open_url_in_default_browser(url.as_str())
                }
            }
        }
    }

    fn open_url_in_default_browser(&self, url: &str) {
        let url = url.to_owned();

        let ctx = glib::MainContext::default();
        ctx.spawn_local(async move {
            if let Err(error) = open_uri::open_uri(&WindowIdentifier::default(), &url, false, false).await {
                Util::send(Action::ErrorSimpleMessage(format!("Failed to open URL: {}", error)));
            }
        });
    }

    fn query_disk_space(&self, oneshot_sender: OneShotSender<UserDataSize>) {
        if let Some(news_flash) = self.news_flash.read().as_ref() {
            if let Ok(db_size) = news_flash.database_size() {
                if let Ok(webkit_size) = news_flash::util::folder_size(&WEBKIT_DATA_DIR) {
                    let user_data_size = UserDataSize {
                        database: db_size,
                        webkit: webkit_size,
                    };
                    oneshot_sender.send(user_data_size).expect(CHANNEL_ERROR);
                }
            }
        }
    }

    fn clear_cache(&self, oneshot_sender: OneShotSender<()>) {
        self.get_main_window()
            .content_page()
            .articleview_column()
            .article_view()
            .clear_cache(oneshot_sender);
    }
}

glib::wrapper! {
    pub struct App(ObjectSubclass<AppPrivate>)
        @extends gio::Application, gtk4::Application, libadwaita::Application;
}

impl App {
    pub fn run(allow_webview_inspector: bool, start_headless: bool) {
        let app: App = glib::Object::new(&[("application-id", &Some(APP_ID)), ("flags", &ApplicationFlags::empty())])
            .expect("Failed to cast to App");

        let private = app.private();
        private
            .settings
            .write()
            .set_inspect_article_view(allow_webview_inspector);
        *private.start_headless.write() = start_headless;
        let empty_args: Vec<&str> = vec![];
        ApplicationExtManual::run_with_args(&app, &empty_args);
    }

    pub fn default() -> App {
        gio::Application::default()
            .expect("Failed to get default gio::Application")
            .downcast::<App>()
            .expect("failed to downcast gio::Application to App")
    }

    pub fn sender(&self) -> Sender<Action> {
        self.private().sender.clone()
    }

    pub fn features(&self) -> Arc<RwLock<PluginCapabilities>> {
        self.private().features.clone()
    }

    pub fn settings(&self) -> Arc<RwLock<Settings>> {
        self.private().settings.clone()
    }

    pub fn content_page_state(&self) -> Arc<RwLock<ContentPageState>> {
        self.private().get_main_window().content_page().state()
    }

    pub fn threadpool(&self) -> ThreadPool {
        self.private().threadpool.clone()
    }

    pub fn news_flash(&self) -> Arc<RwLock<Option<NewsFlash>>> {
        self.private().news_flash.clone()
    }

    fn private(&self) -> &AppPrivate {
        &AppPrivate::from_instance(self)
    }

    pub fn desktop_settings(&self) -> &DesktopSettings {
        &AppPrivate::from_instance(self).desktop_settings
    }

    pub fn set_newsflash_error(&self, error: NewsFlashError) {
        self.private().news_flash_error.write().replace(error);
    }

    fn setup_actions(&self) {
        let main_window = self.private().get_main_window();

        // -------------------------
        // rename category
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let rename_category_dialog_action = SimpleAction::new("rename-category-dialog", Some(&type_));
        rename_category_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(category_id_str) = parameter.map(|p| p.str()).flatten() {
                    let category_id = CategoryID::new(category_id_str);
                    this.rename_category_dialog(Arc::new(FeedListItemID::Category(category_id)));
                } else {
                    Util::send(Action::ErrorSimpleMessage("Rename category: no parameter".into()));
                }
            }),
        );

        // -------------------------
        // delete category
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_category_action = SimpleAction::new("enqueue-delete-category", Some(&type_));
        delete_category_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let category_id = CategoryID::new(id_str);
                    this.private().get_main_window().show_undo_bar(UndoAction::DeleteCategory(category_id, name_str.into()));
                }
            } else {
                Util::send(Action::ErrorSimpleMessage("Delete category: no parameter".into()));
            }
        }));

        // -------------------------
        // rename feed
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let rename_feed_dialog_action = SimpleAction::new("rename-feed-dialog", Some(&type_));
        rename_feed_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(parameter) = parameter {
                    let id_value = parameter.child_value(0);
                    let parent_value = parameter.child_value(1);

                    if let (Some(id_str), Some(parent_str)) = (id_value.str(), parent_value.str()) {
                        let feed_id = FeedID::new(id_str);
                        let parent_id = CategoryID::new(parent_str);
                        this.rename_feed_dialog(Arc::new(FeedListItemID::Feed(feed_id, parent_id)));
                    }
                } else {
                    Util::send(Action::ErrorSimpleMessage("Rename feed: no parameter".into()));
                }
            }),
        );

        // -------------------------
        // delete feed
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_feed_action = SimpleAction::new("enqueue-delete-feed", Some(&type_));
        delete_feed_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let feed_id = FeedID::new(id_str);
                    this.private().get_main_window().show_undo_bar(UndoAction::DeleteFeed(feed_id, name_str.into()));
                }
            } else {
                Util::send(Action::ErrorSimpleMessage("Delete feed: no parameter".into()));
            }
        }));

        // -------------------------
        // rename tag
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let rename_tag_dialog_action = SimpleAction::new("rename-tag-dialog", Some(&type_));
        rename_tag_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.map(|p| p.str()).flatten() {
                    let tag_id = TagID::new(id_str);
                    this.rename_tag_dialog(Arc::new(tag_id));
                }
            }),
        );

        // -------------------------
        // delete tag
        // -------------------------
        let type_ = glib::VariantType::new("(ss)").unwrap();
        let delete_tag_action = SimpleAction::new("enqueue-delete-tag", Some(&type_));
        delete_tag_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(parameter) = parameter {
                let id_value = parameter.child_value(0);
                let name_value = parameter.child_value(1);

                if let (Some(id_str), Some(name_str)) = (id_value.str(), name_value.str()) {
                    let tag_id = TagID::new(id_str);
                    this.private().get_main_window().show_undo_bar(UndoAction::DeleteTag(tag_id, name_str.into()));
                }
            } else {
                Util::send(Action::ErrorSimpleMessage("Delete Tag: no parameter".into()));
            }
        }));

        // -------------------------
        // show shortcuts dialog
        // -------------------------
        let show_shortcut_window_action = SimpleAction::new("shortcut-window", None);
        show_shortcut_window_action.connect_activate(|_action, _parameter| {
            Util::send(Action::ShowShortcutWindow);
        });

        // -------------------------
        // about dialog
        // -------------------------
        let show_about_window_action = SimpleAction::new("about-window", None);
        show_about_window_action.connect_activate(|_action, _parameter| {
            Util::send(Action::ShowAboutWindow);
        });

        // -------------------------
        // show settings dialog
        // -------------------------
        let settings_window_action = SimpleAction::new("settings", None);
        settings_window_action.connect_activate(|_action, _parameter| {
            Util::send(Action::ShowSettingsWindow);
        });

        // -------------------------
        // show discover dialog
        // -------------------------
        let discover_dialog_action = SimpleAction::new("discover", None);
        discover_dialog_action.connect_activate(|_action, _parameter| {
            Util::send(Action::ShowDiscoverDialog);
        });
        discover_dialog_action.set_enabled(self.features().read().contains(PluginCapabilities::ADD_REMOVE_FEEDS));

        // -------------------------
        // quit app
        // -------------------------
        let quit_action = SimpleAction::new("quit-application", None);
        quit_action.connect_activate(|_action, _parameter| {
            Util::send(Action::QueueQuit);
        });

        // -------------------------
        // import opml
        // -------------------------
        let import_opml_action = SimpleAction::new("import-opml", None);
        import_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.private().import_opml();
            }),
        );

        // -------------------------
        // export opml
        // -------------------------
        let export_opml_action = SimpleAction::new("export-opml", None);
        export_opml_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.private().export_opml();
            }),
        );

        // -------------------------
        // reset account
        // -------------------------
        let reset_account_action = SimpleAction::new("reset-account", None);
        reset_account_action.connect_activate(|_action, _parameter| {
            Util::send(Action::ShowResetPage);
        });

        // -------------------------
        // update account login
        // -------------------------
        let update_login_action = SimpleAction::new("update-login", None);
        update_login_action.connect_activate(|_action, _parameter| {
            Util::send(Action::UpdateLogin);
        });

        // -------------------------
        // go offline
        // -------------------------
        let offline_action = SimpleAction::new("go-offline", None);
        offline_action.connect_activate(|_action, _parameter| {
            Util::send(Action::SetOfflineMode(true));
        });

        // -------------------------
        // close article
        // -------------------------
        let close_article_action = SimpleAction::new("close-article", None);
        close_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let main_window = this.private().get_main_window();
                let article_view_column = main_window.content_page().articleview_column();

                // clear view
                article_view_column.article_view().close_article();
                // update headerbar
                article_view_column.show_article(None, None);
            }),
        );

        // -------------------------
        // export article
        // -------------------------
        let export_article_action = SimpleAction::new("export-article", None);
        export_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.private().export_article();
            }),
        );

        // -------------------------
        // open article in browser
        // -------------------------
        let open_selected_article_action = SimpleAction::new("open-selected-article-in-browser", None);
        open_selected_article_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.private().open_selected_article_in_browser();
            }),
        );

        let type_ = glib::VariantType::new("s").unwrap();
        let open_article_action = SimpleAction::new("open-article-in-browser", Some(&type_));
        open_article_action.connect_activate(clone!(@weak self as this => @default-panic, move |_action, parameter| {
            if let Some(id_str) = parameter.map(|p| p.str()).flatten() {
                let article_id = ArticleID::new(id_str);
                this.private().open_article_in_browser(&article_id);
            }
        }));

        // -------------------------
        // toggle article read
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_read_action = SimpleAction::new("toggle-article-read", Some(&type_));
        toggle_article_read_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.map(|p| p.str()).flatten() {
                    let article_id = ArticleID::new(id_str);
                    this.private().toggle_article_read(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // toggle article marked
        // -------------------------
        let type_ = glib::VariantType::new("s").unwrap();
        let toggle_article_marked_action = SimpleAction::new("toggle-article-marked", Some(&type_));
        toggle_article_marked_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, parameter| {
                if let Some(id_str) = parameter.map(|p| p.str()).flatten() {
                    let article_id = ArticleID::new(id_str);
                    this.private().toggle_article_marked(&Arc::new(article_id));
                }
            }),
        );

        // -------------------------
        // show error dialog
        // -------------------------
        let show_error_dialog_action = SimpleAction::new("show-error-dialog", None);
        show_error_dialog_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                let error = this.private().news_flash_error.write().take();
                if let Some(error) = error {
                    let dialog = ErrorDialog::new();
                    dialog.set_transient_for(Some(&*this.private().get_main_window()));
                    dialog.set_error(&error);
                }
            }),
        );

        // ---------------------------
        // remove current undo action
        // ---------------------------
        let remove_undo_action = SimpleAction::new("remove-undo-action", None);
        remove_undo_action.connect_activate(
            clone!(@weak self as this => @default-panic, move |_action, _parameter| {
                this.private().get_main_window().content_page().remove_current_undo_action();
            }),
        );

        main_window.add_action(&delete_category_action);
        main_window.add_action(&rename_category_dialog_action);
        main_window.add_action(&delete_feed_action);
        main_window.add_action(&rename_feed_dialog_action);
        main_window.add_action(&delete_tag_action);
        main_window.add_action(&rename_tag_dialog_action);
        main_window.add_action(&show_shortcut_window_action);
        main_window.add_action(&show_about_window_action);
        main_window.add_action(&settings_window_action);
        main_window.add_action(&discover_dialog_action);
        main_window.add_action(&quit_action);
        main_window.add_action(&import_opml_action);
        main_window.add_action(&export_opml_action);
        main_window.add_action(&reset_account_action);
        main_window.add_action(&update_login_action);
        main_window.add_action(&offline_action);
        main_window.add_action(&close_article_action);
        main_window.add_action(&open_selected_article_action);
        main_window.add_action(&open_article_action);
        main_window.add_action(&export_article_action);
        main_window.add_action(&toggle_article_read_action);
        main_window.add_action(&toggle_article_marked_action);
        main_window.add_action(&show_error_dialog_action);
        main_window.add_action(&remove_undo_action);
    }

    fn process_action(&self, action: Action) -> glib::Continue {
        let private = self.private();

        match action {
            Action::ErrorSimpleMessage(msg) => private.get_main_window().content_page().simple_error(&msg),
            Action::Error(msg, error) => private.get_main_window().content_page().newsflash_error(&msg, error),
            Action::LoadFavIcon((feed_id, sender)) => private.load_favicon(feed_id, sender),
            Action::LoadThumbnail((article_id, sender)) => private.load_thumbnail(article_id, sender),
            Action::ShowWelcomePage => private.get_main_window().show_welcome_page(),
            Action::ShowContentPage => private.get_main_window().show_content_page(),
            Action::ShowPasswordLogin(plugin_id, data, prev_page) => private
                .get_main_window()
                .show_password_login_page(&plugin_id, data, prev_page),
            Action::ShowOauthLogin(plugin_id, prev_page) => {
                private.get_main_window().show_oauth_login_page(&plugin_id, prev_page)
            }
            Action::ShowResetPage => private.get_main_window().show_reset_page(),
            Action::ShowDiscoverDialog => private.spawn_discover_dialog(),
            Action::ShowSettingsWindow => private.spawn_settings_window(),
            Action::ShowShortcutWindow => private.spawn_shortcut_window(),
            Action::ShowAboutWindow => private.spawn_about_window(),
            Action::CancelReset => private.get_main_window().cancel_reset(),
            Action::Login(data) => private.login(data),
            Action::UpdateLogin => private.update_login(),
            Action::ResetAccount => private.reset_account(),
            Action::ResetAccountError(error) => private.get_main_window().reset_account_failed(error),
            Action::ScheduleSync => private.schedule_sync(),
            Action::Sync => self.sync(),
            Action::InitSync => self.init_sync(),
            Action::MarkArticleRead(update) => private.mark_article_read(update),
            Action::MarkArticle(update) => private.mark_article(update),
            Action::ToggleArticleRead => private.toggle_selected_article_read(),
            Action::ToggleArticleMarked => private.toggle_selected_article_marked(),
            Action::UpdateSidebar => private.get_main_window().content_page().update_sidebar(),
            Action::UpdateArticleList => private.get_main_window().content_page().update_article_list(),
            Action::LoadMoreArticles => private.get_main_window().content_page().load_more_articles(),
            Action::SidebarSelection(selection) => private.get_main_window().sidebar_selection(selection),
            Action::SelectNextArticle => private
                .get_main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_next_article(),
            Action::SelectPrevArticle => private
                .get_main_window()
                .content_page()
                .article_list_column()
                .article_list()
                .select_prev_article(),
            Action::UpdateArticleHeader => private.get_main_window().update_article_header(),
            Action::ShowArticle(article_id) => private.get_main_window().show_article(article_id),
            Action::RedrawArticle => private
                .get_main_window()
                .content_page()
                .articleview_column()
                .article_view()
                .redraw_article(),
            Action::SearchTerm(search_term) => private.get_main_window().set_search_term(search_term),
            Action::SetSidebarRead => private.get_main_window().set_sidebar_read(),
            Action::AddFeed((url, title, category)) => private.add_feed(url, title, category),
            Action::AddCategory(title) => private.add_category(title),
            Action::AddTag(color, title) => private.add_tag(color, title),
            Action::DeleteSidebarSelection => private.delete_selection(),
            Action::DeleteFeed(feed_id) => private.delete_feed(feed_id),
            Action::DeleteCategory(category_id) => private.delete_category(category_id),
            Action::DeleteTag(tag_id) => private.delete_tag(tag_id),
            Action::TagArticle(article_id, tag_id) => private.tag_article(article_id, tag_id),
            Action::UntagArticle(article_id, tag_id) => private.untag_article(article_id, tag_id),
            Action::DragAndDrop(action) => private.drag_and_drop(action),
            Action::StartGrabArticleContent => private.start_grab_article_content(),
            Action::MercuryGrabArticleContent(article, next_page_url, previous_urls) => {
                private.mercury_grab_article_contnet(article, next_page_url, previous_urls)
            }
            Action::FinishGrabArticleContent(article_id, article, update_db) => {
                private.finish_grab_article_content(article_id, article, update_db)
            }
            Action::QueueQuit => private.queue_quit(),
            Action::SetOfflineMode(offline) => private.set_offline(offline),
            Action::IgnoreTLSErrors => private.ignore_tls_errors(),
            Action::OpenSelectedArticle => private.open_selected_article_in_browser(),
            Action::OpenUrlInDefaultBrowser(url) => private.open_url_in_default_browser(&url),
            Action::QueryDiskSpace(oneshot) => private.query_disk_space(oneshot),
            Action::ClearCache(oneshot) => private.clear_cache(oneshot),
        }
        glib::Continue(true)
    }

    fn rename_feed_dialog(&self, id: Arc<FeedListItemID>) {
        if let Some(news_flash) = self.news_flash().read().as_ref() {
            let (feeds, _mappings) = match news_flash.get_feeds() {
                Ok(result) => result,
                Err(error) => {
                    let message = "Failed to laod list of feeds.".to_owned();
                    Util::send(Action::Error(message, error));
                    return;
                }
            };

            let feed_id = match &*id {
                FeedListItemID::Feed(feed_id, _parent_id) => feed_id.clone(),
                _ => panic!(),
            };
            let feed = match feeds.iter().find(|f| f.feed_id == feed_id).cloned() {
                Some(feed) => feed,
                None => {
                    let message = format!("Failed to find feed '{}'", feed_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    return;
                }
            };

            let dialog = RenameDialog::new(
                &*self.private().get_main_window(),
                &SidebarSelection::FeedList(id, feed.label.clone()),
            );

            dialog.connect_rename(clone!(@weak self as this => @default-panic, move |dialog| {
                let new_label = dialog.text();
                if new_label.is_empty() {
                    Util::send(Action::ErrorSimpleMessage("No valid title to rename feed.".to_owned()));
                    dialog.close();
                    return;
                }

                this.private().rename_feed(&feed, &new_label);
                dialog.close();
            }));
        }
    }

    fn rename_category_dialog(&self, id: Arc<FeedListItemID>) {
        if let Some(news_flash) = self.news_flash().read().as_ref() {
            let categories = match news_flash.get_categories() {
                Ok(categories) => categories,
                Err(error) => {
                    let message = "Failed to load list of categories.".to_owned();
                    Util::send(Action::Error(message, error));
                    return;
                }
            };

            let category_id = match &*id {
                FeedListItemID::Category(category_id) => category_id.clone(),
                _ => panic!(),
            };
            let category = match categories.iter().find(|c| c.category_id == category_id).cloned() {
                Some(category) => category,
                None => {
                    let message = format!("Failed to find category '{}'", category_id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    return;
                }
            };

            let dialog = RenameDialog::new(
                &*self.private().get_main_window(),
                &SidebarSelection::FeedList(id, category.label.clone()),
            );

            dialog.connect_rename(clone!(@weak self as this => @default-panic, move |dialog| {
                let new_label = dialog.text();
                if new_label.is_empty() {
                    Util::send(Action::ErrorSimpleMessage(
                        "No valid title to rename category.".to_owned(),
                    ));
                    dialog.close();
                    return;
                }

                let category = category.clone();
                this.private().rename_category(category, new_label);
                dialog.close();
            }));
        }
    }

    fn rename_tag_dialog(&self, id: Arc<TagID>) {
        if let Some(news_flash) = self.news_flash().read().as_ref() {
            let tags = match news_flash.get_tags() {
                Ok(result) => result,
                Err(error) => {
                    let message = "Failed to laod list of tags.".to_owned();
                    Util::send(Action::Error(message, error));
                    return;
                }
            };

            let tag = match tags.iter().find(|t| t.tag_id == *id).cloned() {
                Some(tag) => tag,
                None => {
                    let message = format!("Failed to find tag '{}'", *id);
                    Util::send(Action::ErrorSimpleMessage(message));
                    return;
                }
            };

            let dialog = RenameDialog::new(
                &*self.private().get_main_window(),
                &SidebarSelection::Tag(id, tag.label.clone()),
            );

            dialog.connect_rename(clone!(@weak self as this => @default-panic, move |dialog| {
                let new_label = dialog.text();
                if new_label.is_empty() {
                    Util::send(Action::ErrorSimpleMessage("No valid title to rename tag.".to_owned()));
                    dialog.close();
                    return;
                }

                this.private().rename_tag(&tag, &new_label);
                dialog.close();
            }));
        }
    }

    fn sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.private().get_main_window().content_page().start_sync();

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.sync(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            if let Some(news_flash) = this.news_flash().read().as_ref() {
                let unread_count = match news_flash.unread_count_all() {
                    Ok(unread_count) => unread_count,
                    Err(_) => 0,
                };
                match res {
                    Ok(Ok(new_article_count)) => {
                        this.private().get_main_window().content_page().finish_sync();
                        Util::send(Action::UpdateSidebar);
                        Util::send(Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        this.show_notification(counts);
                    }
                    Ok(Err(error)) => {
                        this.private().get_main_window().content_page().finish_sync();
                        Util::send(Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn init_sync(&self) {
        let (sender, receiver) = oneshot::channel::<Result<i64, NewsFlashError>>();
        self.private().get_main_window().content_page().start_sync();

        // features might have changed after login
        // - check if discover is allowed
        // - update add popover features
        if let Some(discover_dialog_action) = self.private().get_main_window().lookup_action("discover") {
            discover_dialog_action
                .downcast::<SimpleAction>()
                .expect("downcast Action to SimpleAction")
                .set_enabled(self.features().read().contains(PluginCapabilities::ADD_REMOVE_FEEDS));
        }
        self.private()
            .get_main_window()
            .content_page()
            .sidebar_column()
            .refresh_app_popover_features();

        let thread_future = async move {
            if let Some(news_flash) = App::default().news_flash().read().as_ref() {
                let result = Runtime::new()
                    .expect(RUNTIME_ERROR)
                    .block_on(news_flash.initial_sync(&Util::build_client()));
                sender.send(result).expect(CHANNEL_ERROR);
            }
        };

        let glib_future = receiver.map(clone!(
            @weak self as this => @default-panic, move |res|
        {
            if let Some(news_flash) = this.news_flash().read().as_ref() {
                let unread_count = match news_flash.unread_count_all() {
                    Ok(unread_count) => unread_count,
                    Err(_) => 0,
                };
                match res {
                    Ok(Ok(new_article_count)) => {
                        this.private().get_main_window().content_page().finish_sync();
                        Util::send(Action::UpdateSidebar);
                        Util::send(Action::UpdateArticleList);
                        let counts = NotificationCounts {
                            new: new_article_count,
                            unread: unread_count,
                        };
                        this.show_notification(counts);
                    }
                    Ok(Err(error)) => {
                        this.private().get_main_window().content_page().finish_sync();
                        Util::send(Action::Error("Failed to sync.".to_owned(), error));
                    }
                    Err(_) => {}
                }
            }
        }));

        self.threadpool().spawn_ok(thread_future);
        Util::glib_spawn_future(glib_future);
    }

    fn show_notification(&self, counts: NotificationCounts) {
        // don't notify when window is visible and focused
        let window = self.private().get_main_window();
        if window.is_visible() && window.is_active() {
            return;
        }

        if counts.new > 0 && counts.unread > 0 {
            let summary = i18n("New Articles");

            let message = if counts.new == 1 {
                i18n_f("There is 1 new article ({} unread)", &[&counts.unread.to_string()])
            } else {
                i18n_f(
                    "There are {} new articles ({} unread)",
                    &[&counts.new.to_string(), &counts.unread.to_string()],
                )
            };

            let notification = Notification::new(&summary);
            notification.set_body(Some(&message));
            notification.set_priority(NotificationPriority::Normal);
            notification.set_icon(&ThemedIcon::new(APP_ID));

            self.send_notification(Some("newsflash_sync"), &notification);
        }
    }
}
