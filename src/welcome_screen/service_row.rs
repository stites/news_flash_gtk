use crate::i18n::i18n;
use crate::util::GtkUtil;
use glib::clone;
use gtk4::{
    self, prelude::*, subclass::prelude::*, CompositeTemplate, EventControllerMotion, GestureClick, Image, Label,
    ListBoxRow, Revealer, Widget,
};
use news_flash::models::{PluginIcon, PluginInfo, ServiceLicense, ServicePrice, ServiceType};
use parking_lot::RwLock;
use std::sync::Arc;

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/service_row.ui")]
    pub struct ServiceRow {
        #[template_child]
        pub row_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub arrow_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub arrow_image: TemplateChild<Image>,
        #[template_child]
        pub arrow_image_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub arrow_image_click: TemplateChild<GestureClick>,
        #[template_child]
        pub info_revealer: TemplateChild<Revealer>,
        #[template_child]
        pub label: TemplateChild<Label>,
        #[template_child]
        pub icon: TemplateChild<Image>,
        #[template_child]
        pub website_label: TemplateChild<Label>,
        #[template_child]
        pub license_label: TemplateChild<Label>,
        #[template_child]
        pub type_label: TemplateChild<Label>,
        #[template_child]
        pub price_label: TemplateChild<Label>,

        pub show_info: Arc<RwLock<bool>>,
    }

    impl Default for ServiceRow {
        fn default() -> Self {
            Self {
                row_motion: TemplateChild::default(),
                arrow_revealer: TemplateChild::default(),
                arrow_image: TemplateChild::default(),
                arrow_image_motion: TemplateChild::default(),
                arrow_image_click: TemplateChild::default(),
                info_revealer: TemplateChild::default(),
                label: TemplateChild::default(),
                icon: TemplateChild::default(),
                website_label: TemplateChild::default(),
                license_label: TemplateChild::default(),
                type_label: TemplateChild::default(),
                price_label: TemplateChild::default(),

                show_info: Arc::new(RwLock::new(false)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ServiceRow {
        const NAME: &'static str = "ServiceRow";
        type ParentType = ListBoxRow;
        type Type = super::ServiceRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ServiceRow {}

    impl WidgetImpl for ServiceRow {}

    impl ListBoxRowImpl for ServiceRow {}
}

glib::wrapper! {
    pub struct ServiceRow(ObjectSubclass<imp::ServiceRow>)
        @extends Widget, ListBoxRow;
}

impl ServiceRow {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self, info: PluginInfo) {
        let imp = imp::ServiceRow::from_instance(self);

        imp.label.set_label(&info.name);

        // Website
        if let Some(website) = info.website {
            let website_string = website.to_string();
            imp.website_label
                .set_markup(&format!("<a href=\"{}\">{}</a>", website_string, website_string));
        } else {
            imp.website_label.set_text(&i18n("No Website"));
        }

        let unknown = i18n("Unknown Free License");
        let sad_panda = i18n("Proprietary Software");
        let license_string = match info.license_type {
            ServiceLicense::ApacheV2 => "<a href=\"http://www.apache.org/licenses/LICENSE-2.0\">Apache v2</a>",
            ServiceLicense::AGPLv3 => "<a href=\"https://www.gnu.org/licenses/agpl-3.0.en.html\">AGPLv3</a>",
            ServiceLicense::GPLv2 => "<a href=\"http://www.gnu.de/documents/gpl-2.0.en.html\">GPLv2</a>",
            ServiceLicense::GPLv3 => "<a href=\"http://www.gnu.de/documents/gpl-3.0.en.html\">GPLv3</a>",
            ServiceLicense::MIT => "<a href=\"https://opensource.org/licenses/MIT\">MIT</a>",
            ServiceLicense::LGPLv21 => "<a href=\"http://www.gnu.de/documents/lgpl-2.1.en.html\">LGPLv2.1</a>",
            ServiceLicense::GenericFree => &unknown,
            ServiceLicense::GenericProprietary => &sad_panda,
        };
        imp.license_label.set_markup(license_string);

        let type_string = match info.service_type {
            ServiceType::Local => i18n("Local data only"),
            ServiceType::Remote { self_hosted } => {
                if self_hosted {
                    i18n("Synced & self hosted")
                } else {
                    i18n("Synced")
                }
            }
        };
        imp.type_label.set_text(&type_string);

        let price_string = match info.service_price {
            ServicePrice::Free => i18n("Free"),
            ServicePrice::Paid => i18n("Paid"),
            ServicePrice::PaidPremimum => i18n("Free / Paid Premium"),
        };
        imp.price_label.set_text(&price_string);

        imp.icon.set_from_icon_name(Some("feed-service-generic"));
        let scale = GtkUtil::get_scale(self);
        if let Some(icon) = info.icon {
            let texture = match icon {
                PluginIcon::Vector(icon) => GtkUtil::create_texture_from_bytes(&icon.data, 64, 64, scale)
                    .expect("Failed to create surface from service vector icon."),
                PluginIcon::Pixel(icon) => GtkUtil::create_texture_from_pixelicon(&icon, scale)
                    .expect("Failed to create surface from service pixel icon."),
            };
            imp.icon.set_from_paintable(Some(&texture));
        }

        self.setup_events();
    }

    fn setup_events(&self) {
        let imp = imp::ServiceRow::from_instance(self);

        let arrow_image = imp.arrow_image.get();
        imp.arrow_image_motion.connect_leave(move |_controller| {
            arrow_image.set_opacity(0.8);
        });
        let arrow_image = imp.arrow_image.get();
        imp.arrow_image_motion.connect_enter(move |_controller, _x, _y| {
            arrow_image.set_opacity(1.0);
        });

        let arrow_revealer = imp.arrow_revealer.get();
        imp.row_motion.connect_enter(move |_controller, _x, _y| {
            arrow_revealer.set_reveal_child(true);
        });

        imp.row_motion.connect_leave(clone!(
            @strong imp.arrow_revealer as arrow_revealer,
            @strong imp.show_info as show_info => @default-panic, move |_controller| {
                arrow_revealer.set_reveal_child(false);
        }));

        imp.arrow_image_click.connect_pressed(clone!(
            @weak self as this => @default-panic, move |_gesture, times, _x, _y|
        {
            if times != 1 {
                return
            }

            let imp = imp::ServiceRow::from_instance(&this);
            let context = imp.arrow_image.style_context();
            let expanded = *imp.show_info.read();
            if !expanded {
                context.add_class("backward-arrow-expanded");
                context.remove_class("backward-arrow-collapsed");
                imp.info_revealer.set_reveal_child(true);
            } else {
                context.remove_class("backward-arrow-expanded");
                context.add_class("backward-arrow-collapsed");
                imp.info_revealer.set_reveal_child(false);
            }
            *imp.show_info.write() = !expanded;
        }));
    }
}
