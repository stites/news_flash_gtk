use crate::util::{constants, GtkUtil};
use glib::{clone, subclass::*};
use gtk4::{
    prelude::*, subclass::prelude::*, CompositeTemplate, DrawingArea, EventControllerMotion, GestureClick, Image,
    Label, ListBoxRow,
};
use lazy_static::lazy_static;
use news_flash::models::{Tag, TagID};
use parking_lot::RwLock;

lazy_static! {
    pub static ref SIGNALS: Vec<Signal> = vec![
        Signal::builder(
            "remove-tag",
            &[String::static_type().into(), ListBoxRow::static_type().into()],
            <()>::static_type().into(),
        )
        .build(),
        Signal::builder(
            "clicked",
            &[String::static_type().into(), ListBoxRow::static_type().into()],
            <()>::static_type().into(),
        )
        .build()
    ];
}

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/popover_tag.ui")]
    pub struct PopoverTagRow {
        pub id: RwLock<TagID>,

        #[template_child]
        pub tag_click: TemplateChild<GestureClick>,
        #[template_child]
        pub tag_title: TemplateChild<Label>,
        #[template_child]
        pub tag_color: TemplateChild<DrawingArea>,
        #[template_child]
        pub image: TemplateChild<Image>,
        #[template_child]
        pub image_motion: TemplateChild<EventControllerMotion>,
        #[template_child]
        pub image_click: TemplateChild<GestureClick>,
    }

    impl Default for PopoverTagRow {
        fn default() -> Self {
            PopoverTagRow {
                id: RwLock::new(TagID::new("")),
                tag_click: TemplateChild::default(),
                tag_title: TemplateChild::default(),
                tag_color: TemplateChild::default(),
                image: TemplateChild::default(),
                image_motion: TemplateChild::default(),
                image_click: TemplateChild::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PopoverTagRow {
        const NAME: &'static str = "PopoverTagRow";
        type ParentType = gtk4::ListBoxRow;
        type Type = super::PopoverTagRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PopoverTagRow {
        fn signals() -> &'static [Signal] {
            SIGNALS.as_ref()
        }
    }

    impl WidgetImpl for PopoverTagRow {}

    impl ListBoxRowImpl for PopoverTagRow {}
}

glib::wrapper! {
    pub struct PopoverTagRow(ObjectSubclass<imp::PopoverTagRow>)
        @extends gtk4::Widget, gtk4::ListBoxRow;
}

impl PopoverTagRow {
    pub fn new(tag: &Tag, assigned: bool) -> Self {
        let row = glib::Object::new::<Self>(&[]).unwrap();
        let imp = imp::PopoverTagRow::from_instance(&row);

        *imp.id.write() = tag.tag_id.clone();
        imp.image.set_visible(assigned);

        let image = imp.image.get();
        imp.image_motion.connect_enter(move |_event_controller, _x, _y| {
            image.set_opacity(1.0);
        });
        let image = imp.image.get();
        imp.image_motion.connect_leave(move |_event_controller| {
            image.set_opacity(0.6);
        });

        let color = tag.color.clone();
        imp.tag_color.set_draw_func(move |_drawing_area, ctx, _width, _height| {
            let color = match &color {
                Some(color) => color,
                None => constants::TAG_DEFAULT_INNER_COLOR,
            };
            GtkUtil::draw_color_cirlce(&ctx, color);
        });

        imp.tag_title.set_label(&tag.label);

        imp.image_click
            .connect_pressed(clone!(@weak row => @default-panic, move |_gesture, times, _x, _y| {
                if times != 1 {
                    return
                }
                let imp = imp::PopoverTagRow::from_instance(&row);
                row.emit_by_name::<()>("remove-tag", &[&imp.id.read().to_str().to_owned(), &row]);
            }));

        imp.tag_click
            .connect_pressed(clone!(@weak row => @default-panic, move |_gesture, times, _x, _y| {
                if times != 1 {
                    return
                }
                let imp = imp::PopoverTagRow::from_instance(&row);
                row.emit_by_name::<()>("clicked", &[&imp.id.read().to_str().to_owned(), &row]);
            }));

        row
    }

    pub fn id(&self) -> TagID {
        let imp = imp::PopoverTagRow::from_instance(self);
        imp.id.read().clone()
    }
}
