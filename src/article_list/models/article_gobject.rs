use super::ArticleListArticleModel;
use crate::util::DateUtil;
use chrono::{NaiveDateTime, Utc};
use glib::{
    Boxed, Enum, Object, ObjectExt, ParamFlags, ParamSpec, ParamSpecBoxed, ParamSpecEnum, ParamSpecString, StaticType,
    ToValue, Value,
};
use gtk4::subclass::prelude::*;
use lazy_static::lazy_static;
use news_flash::models::{ArticleID, FeedID, Marked, Read};
use parking_lot::RwLock;
use std::cell::{Cell, RefCell};
use std::sync::Arc;

lazy_static! {
    pub static ref PROPERTIES: Vec<ParamSpec> = vec![
        ParamSpecEnum::new("read", "read", "read", GRead::static_type(), 0, ParamFlags::READWRITE),
        ParamSpecEnum::new(
            "marked",
            "marked",
            "marked",
            GMarked::static_type(),
            0,
            ParamFlags::READWRITE
        ),
        ParamSpecString::new("date-string", "date-string", "date-string", None, ParamFlags::READWRITE),
        ParamSpecBoxed::new("date", "date", "date", GDateTime::static_type(), ParamFlags::READWRITE),
    ];
}

mod imp {
    use super::*;

    pub struct ArticleGObject {
        pub id: RwLock<Arc<ArticleID>>,
        pub title: RwLock<Arc<String>>,
        pub feed_id: RwLock<Arc<FeedID>>,
        pub feed_title: RwLock<Arc<String>>,
        pub summary: RwLock<Arc<String>>,

        pub date: RefCell<GDateTime>,
        pub date_string: RefCell<String>,
        pub read: Cell<GRead>,
        pub marked: Cell<GMarked>,
    }

    impl Default for ArticleGObject {
        fn default() -> Self {
            Self {
                id: RwLock::new(Arc::new(ArticleID::new(""))),
                title: RwLock::new(Arc::new("".into())),
                feed_id: RwLock::new(Arc::new(FeedID::new(""))),
                feed_title: RwLock::new(Arc::new("".into())),
                summary: RwLock::new(Arc::new("".into())),

                date: RefCell::new(Utc::now().naive_utc().into()),
                date_string: RefCell::new(DateUtil::format(&Utc::now().naive_utc())),
                read: Cell::new(GRead::Unread),
                marked: Cell::new(GMarked::Unmarked),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArticleGObject {
        const NAME: &'static str = "ArticleGObject";
        type Type = super::ArticleGObject;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for ArticleGObject {
        fn properties() -> &'static [ParamSpec] {
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "read" => {
                    let input = value.get().expect("The value needs to be of type `GRead`.");
                    self.read.replace(input);
                }
                "marked" => {
                    let input = value.get().expect("The value needs to be of type `GMarked`.");
                    self.marked.replace(input);
                }
                "date" => {
                    let input = value.get().expect("The value needs to be of type `GDateTime`.");
                    self.date.replace(input);
                }
                "date-string" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    self.date_string.replace(input);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "read" => self.read.get().to_value(),
                "marked" => self.marked.get().to_value(),
                "date" => self.date.borrow().to_value(),
                "date-string" => self.date_string.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct ArticleGObject(ObjectSubclass<imp::ArticleGObject>);
}

impl ArticleGObject {
    pub fn new() -> Self {
        Object::new(&[]).expect("Failed to create `CustomButton`.")
    }

    pub fn from_model(model: &ArticleListArticleModel) -> Self {
        let gobject = Self::new();
        let imp = imp::ArticleGObject::from_instance(&gobject);

        *imp.id.write() = model.id.clone();
        *imp.title.write() = model.title.clone();
        *imp.feed_id.write() = model.feed_id.clone();
        *imp.feed_title.write() = model.feed_title.clone();
        *imp.summary.write() = model.summary.clone();

        imp.date.replace(model.date.into());
        imp.date_string.replace(DateUtil::format(&model.date));
        imp.read.replace(model.read.into());
        imp.marked.replace(model.marked.into());
        gobject
    }

    pub fn read(&self) -> GRead {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.read.get()
    }

    pub fn set_gread(&self, g_read: GRead) {
        self.set_property("read", g_read.to_value())
    }

    pub fn set_read(&self, read: Read) {
        let g_read: GRead = read.into();
        self.set_property("read", g_read.to_value())
    }

    pub fn marked(&self) -> GMarked {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.marked.get()
    }

    pub fn set_gmarked(&self, g_marked: GMarked) {
        self.set_property("marked", g_marked.to_value())
    }

    pub fn set_marked(&self, marked: Marked) {
        let g_marked: GMarked = marked.into();
        self.set_property("marked", g_marked.to_value())
    }

    pub fn date(&self) -> GDateTime {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.date.borrow().clone()
    }

    pub fn date_string(&self) -> String {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.date_string.borrow().clone()
    }

    pub fn set_date(&self, date: NaiveDateTime) {
        self.set_property("date-string", DateUtil::format(&date));

        let g_datetime: GDateTime = date.into();
        self.set_property("date", g_datetime.to_value())
    }

    pub fn title(&self) -> Arc<String> {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.title.read().clone()
    }

    pub fn summary(&self) -> Arc<String> {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.summary.read().clone()
    }

    pub fn feed_title(&self) -> Arc<String> {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.feed_title.read().clone()
    }

    pub fn feed_id(&self) -> Arc<FeedID> {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.feed_id.read().clone()
    }

    pub fn article_id(&self) -> Arc<ArticleID> {
        let imp = imp::ArticleGObject::from_instance(self);
        imp.id.read().clone()
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "GRead")]
pub enum GRead {
    Read,
    Unread,
}

impl From<Read> for GRead {
    fn from(read: Read) -> Self {
        match read {
            Read::Read => Self::Read,
            Read::Unread => Self::Unread,
        }
    }
}

impl From<GRead> for Read {
    fn from(read: GRead) -> Self {
        match read {
            GRead::Read => Read::Read,
            GRead::Unread => Read::Unread,
        }
    }
}

impl GRead {
    pub fn invert(&self) -> Self {
        match self {
            Self::Read => Self::Unread,
            Self::Unread => Self::Read,
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy, Enum)]
#[repr(u32)]
#[enum_type(name = "GMarked")]
pub enum GMarked {
    Marked,
    Unmarked,
}

impl From<Marked> for GMarked {
    fn from(marked: Marked) -> Self {
        match marked {
            Marked::Marked => Self::Marked,
            Marked::Unmarked => Self::Unmarked,
        }
    }
}

impl GMarked {
    pub fn invert(&self) -> Self {
        match self {
            Self::Marked => Self::Unmarked,
            Self::Unmarked => Self::Marked,
        }
    }
}

impl From<GMarked> for Marked {
    fn from(marked: GMarked) -> Self {
        match marked {
            GMarked::Marked => Marked::Marked,
            GMarked::Unmarked => Marked::Unmarked,
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Boxed)]
#[boxed_type(name = "GDateTime")]
pub struct GDateTime(NaiveDateTime);

impl From<NaiveDateTime> for GDateTime {
    fn from(dt: NaiveDateTime) -> Self {
        Self(dt)
    }
}

impl From<GDateTime> for NaiveDateTime {
    fn from(dt: GDateTime) -> Self {
        dt.0
    }
}
