use chrono::NaiveDateTime;
use chrono::{Duration, Utc};
use diffus::{Diffable, Same};
use news_flash::models::{Article, ArticleID, Feed, FeedID, Marked, Read};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct ArticleListArticleModel {
    // immutable data
    pub id: Arc<ArticleID>,
    pub title: Arc<String>,
    pub feed_id: Arc<FeedID>,
    pub feed_title: Arc<String>,
    pub summary: Arc<String>,

    // mutable data
    pub date: NaiveDateTime,
    pub read: Read,
    pub marked: Marked,
}

impl ArticleListArticleModel {
    pub fn new(article: Article, feed: &Feed) -> Self {
        let (article_id, title, _author, feed_id, _url, date, summary, _direction, read, marked, _thumbnail_url) =
            article.decompose();

        ArticleListArticleModel {
            id: Arc::new(article_id),
            title: Arc::new(match title {
                Some(title) => title,
                None => "No Title".to_owned(),
            }),
            feed_id: Arc::new(feed_id),
            feed_title: Arc::new(feed.label.clone()),
            summary: Arc::new(match summary {
                Some(summary) => summary,
                None => "No Summary".to_owned(),
            }),
            date,
            read,
            marked,
        }
    }
}

impl PartialEq for ArticleListArticleModel {
    fn eq(&self, other: &ArticleListArticleModel) -> bool {
        self.id == other.id
    }
}

impl Same for ArticleListArticleModel {
    fn same(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<'a> Diffable<'a> for ArticleListArticleModel {
    type Diff = ArticleDiff;

    fn diff(&'a self, other: &'a Self) -> diffus::edit::Edit<Self> {
        let date = if self.date == other.date {
            let diff_days = Utc::now().naive_utc().date() - self.date.date();
            if diff_days > Duration::days(0) && diff_days <= Duration::days(1) {
                Some(other.date)
            } else {
                None
            }
        } else {
            Some(other.date)
        };

        let read = if self.read == other.read {
            None
        } else {
            Some(other.read)
        };

        let marked = if self.marked == other.marked {
            None
        } else {
            Some(other.marked)
        };

        if self == other && date.is_none() && read.is_none() && marked.is_none() {
            diffus::edit::Edit::Copy(self)
        } else {
            diffus::edit::Edit::Change(ArticleDiff {
                id: self.id.clone(),
                read,
                marked,
                date,
            })
        }
    }
}

pub struct ArticleDiff {
    pub id: Arc<ArticleID>,
    pub read: Option<Read>,
    pub marked: Option<Marked>,
    pub date: Option<NaiveDateTime>,
}
