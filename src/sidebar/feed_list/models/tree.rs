use std::collections::HashSet;
use std::sync::Arc;

use super::category::FeedListCategoryModel;
use super::error::{FeedListModelError, FeedListModelErrorKind};
use super::feed::FeedListFeedModel;
use super::item::FeedListItem;
use super::FeedListItemID;
use crate::sidebar::SidebarIterateItem;
use news_flash::models::{Category, CategoryID, Feed, FeedMapping, NEWSFLASH_TOPLEVEL};

#[derive(Clone, Debug)]
pub struct FeedListTree {
    top_level_id: CategoryID,
    pub top_level: Vec<FeedListItem>,
}

impl FeedListTree {
    pub fn new() -> Self {
        FeedListTree {
            top_level_id: NEWSFLASH_TOPLEVEL.clone(),
            top_level: Vec::new(),
        }
    }

    pub fn add_feed(&mut self, feed: &Feed, mapping: &FeedMapping, item_count: i64) -> Result<(), FeedListModelError> {
        if mapping.category_id == self.top_level_id {
            let contains_feed = self.top_level.iter().any(|item| {
                if let FeedListItem::Feed(item) = item {
                    *item.id == FeedListItemID::Feed(feed.feed_id.clone(), mapping.category_id.clone())
                } else {
                    false
                }
            });
            if !contains_feed {
                let feed = FeedListFeedModel::new(feed, mapping, item_count, 0);
                let item = FeedListItem::Feed(feed);
                self.top_level.push(item);
                self.top_level.sort();
            } else {
                return Err(FeedListModelErrorKind::AddDuplicateFeed.into());
            }
            return Ok(());
        }

        if let Some(parent) = self.find_category(&mapping.category_id) {
            let feed = FeedListFeedModel::new(feed, mapping, item_count, parent.level + 1);
            let item = FeedListItem::Feed(feed);
            parent.add_child(item);
            return Ok(());
        }

        Err(FeedListModelErrorKind::AddFeedNoParent.into())
    }

    pub fn add_category(&mut self, category: &Category, item_count: i64) -> Result<(), FeedListModelError> {
        if category.parent_id == self.top_level_id {
            let contains_category = self.top_level.iter().any(|item| {
                if let FeedListItem::Category(item) = item {
                    return *item.id == FeedListItemID::Category(category.category_id.clone());
                }
                false
            });
            if !contains_category {
                let category_ = FeedListCategoryModel::new(&category, item_count, 0);
                let item = FeedListItem::Category(category_);

                self.top_level.push(item);
                self.top_level.sort();
            } else {
                return Err(FeedListModelErrorKind::AddDuplicateCategory.into());
            }
            return Ok(());
        }

        if let Some(parent) = self.find_category(&category.parent_id) {
            let category_ = FeedListCategoryModel::new(&category, item_count, parent.level + 1);
            let item = FeedListItem::Category(category_);
            parent.add_child(item);
            return Ok(());
        }

        Err(FeedListModelErrorKind::AddCategoryNoParent.into())
    }

    fn find_category(&mut self, id: &CategoryID) -> Option<&mut FeedListCategoryModel> {
        let id = Arc::new(FeedListItemID::Category(id.clone()));
        Self::search_subcategories_for_category(&id, &mut self.top_level)
    }

    fn search_subcategories_for_category<'a>(
        id: &Arc<FeedListItemID>,
        children: &'a mut Vec<FeedListItem>,
    ) -> Option<&'a mut FeedListCategoryModel> {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                if category_model.id == *id {
                    return Some(category_model);
                } else if !category_model.children.is_empty() {
                    if let Some(category_model) =
                        Self::search_subcategories_for_category(id, &mut category_model.children)
                    {
                        return Some(category_model);
                    }
                }
            }
        }
        None
    }

    pub fn get_expanded_categories(&mut self) -> HashSet<Arc<FeedListItemID>> {
        let mut expanded = HashSet::new();
        Self::get_expanded_categories_recurse(&mut self.top_level, &mut expanded);
        expanded
    }

    fn get_expanded_categories_recurse(
        children: &'_ mut Vec<FeedListItem>,
        expanded: &mut HashSet<Arc<FeedListItemID>>,
    ) {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                if category_model.expanded {
                    expanded.insert(category_model.id.clone());
                }
                Self::get_expanded_categories_recurse(&mut category_model.children, expanded);
            }
        }
    }

    pub fn apply_expanded_categories(&mut self, expanded: &HashSet<Arc<FeedListItemID>>) {
        Self::apply_expanded_categories_recurse(&mut self.top_level, expanded);
    }

    fn apply_expanded_categories_recurse(children: &'_ mut Vec<FeedListItem>, expanded: &HashSet<Arc<FeedListItemID>>) {
        for item in children {
            if let FeedListItem::Category(category_model) = item {
                category_model.expanded = expanded.contains(&category_model.id);
                Self::apply_expanded_categories_recurse(&mut category_model.children, expanded);
            }
        }
    }

    pub fn set_category_expanded(&mut self, category_id: &CategoryID, expanded: bool) {
        if let Some(category) = self.find_category(category_id) {
            category.set_expanded(expanded);
        }
    }

    pub fn get_category_expanded(&mut self, category_id: &CategoryID) -> bool {
        if let Some(category) = self.find_category(category_id) {
            category.get_expanded()
        } else {
            false
        }
    }

    pub fn calculate_next_item(
        &mut self,
        selected_id: Arc<FeedListItemID>,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        let mut selected_found = false;
        self.top_level.sort();
        Self::iterate_next_internal(&selected_id, &self.top_level, &mut selected_found, only_show_relevant)
    }

    fn iterate_next_internal(
        selected_id: &Arc<FeedListItemID>,
        items: &[FeedListItem],
        selected_found: &mut bool,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        for item in items {
            match item {
                FeedListItem::Feed(feed) => {
                    if !*selected_found {
                        if *selected_id == feed.id {
                            *selected_found = true;
                            continue;
                        }
                    } else if !only_show_relevant || feed.item_count > 0 {
                        return SidebarIterateItem::SelectFeedListItem(feed.id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    if !*selected_found {
                        if *selected_id == category.id {
                            *selected_found = true;
                            if !category.expanded {
                                continue;
                            }
                        }
                    } else {
                        return SidebarIterateItem::SelectFeedListItem(category.id.clone());
                    }

                    match Self::iterate_next_internal(
                        selected_id,
                        &category.children,
                        selected_found,
                        only_show_relevant,
                    ) {
                        SidebarIterateItem::SelectFeedListItem(category) => {
                            return SidebarIterateItem::SelectFeedListItem(category)
                        }
                        _ => {}
                    }
                }
            }
        }
        SidebarIterateItem::TagListSelectFirstItem
    }

    pub fn calculate_prev_item(
        &mut self,
        selected_id: Arc<FeedListItemID>,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        let mut selected_found = false;
        self.top_level.sort();
        Self::iterate_prev_internal(&selected_id, &self.top_level, &mut selected_found, only_show_relevant)
    }

    fn iterate_prev_internal(
        selected_id: &Arc<FeedListItemID>,
        items: &[FeedListItem],
        selected_found: &mut bool,
        only_show_relevant: bool,
    ) -> SidebarIterateItem {
        for item in items.iter().rev() {
            match item {
                FeedListItem::Feed(feed) => {
                    if !*selected_found {
                        if *selected_id == feed.id {
                            *selected_found = true;
                            continue;
                        }
                    } else if !only_show_relevant || feed.item_count > 0 {
                        return SidebarIterateItem::SelectFeedListItem(feed.id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    if category.expanded {
                        match Self::iterate_prev_internal(
                            selected_id,
                            &category.children,
                            selected_found,
                            only_show_relevant,
                        ) {
                            SidebarIterateItem::SelectFeedListItem(category) => {
                                return SidebarIterateItem::SelectFeedListItem(category)
                            }
                            _ => {}
                        }
                    }

                    if !*selected_found {
                        if *selected_id == category.id {
                            *selected_found = true;
                            if !category.expanded {
                                continue;
                            }
                        }
                    } else {
                        return SidebarIterateItem::SelectFeedListItem(category.id.clone());
                    }
                }
            }
        }
        SidebarIterateItem::SelectAll
    }

    pub fn get_first(&mut self, only_show_relevant: bool) -> Option<Arc<FeedListItemID>> {
        self.top_level.sort();

        for item in &self.top_level {
            match item {
                FeedListItem::Feed(feed) => {
                    if only_show_relevant && feed.item_count > 0 {
                        return Some(feed.id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    return Some(category.id.clone());
                }
            }
        }
        None
    }

    pub fn get_last(&mut self, only_show_relevant: bool) -> Option<Arc<FeedListItemID>> {
        self.top_level.sort();
        Self::get_last_internal(&self.top_level, only_show_relevant)
    }

    fn get_last_internal(items: &[FeedListItem], only_show_relevant: bool) -> Option<Arc<FeedListItemID>> {
        for item in items.iter().rev() {
            match item {
                FeedListItem::Feed(feed) => {
                    if only_show_relevant && feed.item_count > 0 {
                        return Some(feed.id.clone());
                    }
                }
                FeedListItem::Category(category) => {
                    if category.expanded {
                        match Self::get_last_internal(&category.children, only_show_relevant) {
                            Some(last_item) => return Some(last_item),
                            None => return Some(category.id.clone()),
                        }
                    }

                    return Some(category.id.clone());
                }
            }
        }
        None
    }

    pub fn get_item_pos(&self, id: Arc<FeedListItemID>, only_show_relevant: bool) -> Option<u32> {
        let mut pos = 0;
        let mut id_found = false;
        Self::get_item_pos_internal(&id, &self.top_level, &mut id_found, &mut pos, only_show_relevant);
        if id_found {
            Some(pos)
        } else {
            None
        }
    }

    fn get_item_pos_internal(
        id: &Arc<FeedListItemID>,
        items: &[FeedListItem],
        id_found: &mut bool,
        pos: &mut u32,
        only_show_relevant: bool,
    ) {
        for item in items {
            match item {
                FeedListItem::Feed(feed) => {
                    if !only_show_relevant || feed.item_count > 0 {
                        if feed.id == *id {
                            *id_found = true;
                            return;
                        }
                    }
                    *pos += 1;
                }
                FeedListItem::Category(category) => {
                    if category.id == *id {
                        *id_found = true;
                        return;
                    }

                    *pos += 1;

                    if category.expanded {
                        Self::get_item_pos_internal(id, &category.children, id_found, pos, only_show_relevant);
                        if *id_found {
                            return;
                        }
                    }
                }
            }
        }
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        self.print_internal(&self.top_level, &mut 0);
    }

    fn print_internal(&self, category: &[FeedListItem], level: &mut i32) {
        let mut new_level = *level + 1;
        for item in category {
            for _ in 0..new_level {
                print!("-- ");
            }

            match item {
                FeedListItem::Category(model) => {
                    println!("{}", model);
                    self.print_internal(&model.children, &mut new_level);
                }
                FeedListItem::Feed(model) => {
                    println!("{}", model);
                }
            }
        }
    }
}
