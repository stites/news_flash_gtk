use super::{FeedListCategoryModel, FeedListFeedModel, FeedListItem, FeedListItemID};
use gio::{prelude::*, ListModel, ListStore};
use glib::{
    Cast, Object, ObjectExt, ParamFlags, ParamSpec, ParamSpecString, ParamSpecUInt, StaticType, ToValue, Value,
};
use gtk4::subclass::prelude::*;
use lazy_static::lazy_static;
use news_flash::models::CategoryID;
use parking_lot::RwLock;
use std::cell::Cell;
use std::collections::HashMap;
use std::sync::Arc;

lazy_static! {
    pub static ref PROPERTIES: Vec<ParamSpec> = vec![
        ParamSpecUInt::new(
            "item-count",
            "item-count",
            "item-count",
            u32::MIN,
            u32::MAX,
            0,
            ParamFlags::READWRITE
        ),
        ParamSpecString::new("label", "label", "label", Some(""), ParamFlags::READWRITE),
    ];
}

mod imp {
    use super::*;

    pub struct FeedListItemGObject {
        pub id: RwLock<Arc<FeedListItemID>>,
        pub parent_id: RwLock<Arc<CategoryID>>,
        pub item_count: Cell<u32>,
        pub label: RwLock<Arc<String>>,
        pub sort_index: Cell<i32>,
        pub list_store: RwLock<Option<ListStore>>,
    }

    impl Default for FeedListItemGObject {
        fn default() -> Self {
            Self {
                id: RwLock::new(Arc::new(FeedListItemID::Category(CategoryID::new("")))),
                parent_id: RwLock::new(Arc::new(CategoryID::new(""))),
                item_count: Cell::new(0),
                label: RwLock::new(Arc::new("".into())),
                sort_index: Cell::new(0),
                list_store: RwLock::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for FeedListItemGObject {
        const NAME: &'static str = "FeedListItemGObject";
        type Type = super::FeedListItemGObject;
        type ParentType = glib::Object;
    }

    impl ObjectImpl for FeedListItemGObject {
        fn properties() -> &'static [ParamSpec] {
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _obj: &Self::Type, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "item-count" => {
                    let input = value.get().expect("The value needs to be of type `u32`.");
                    self.item_count.set(input);
                }
                "label" => {
                    let input = value.get().expect("The value needs to be of type `string`.");
                    *self.label.write() = Arc::new(input);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _obj: &Self::Type, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "item-count" => self.item_count.get().to_value(),
                "label" => (*self.label.read()).to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct FeedListItemGObject(ObjectSubclass<imp::FeedListItemGObject>);
}

impl FeedListItemGObject {
    pub fn new() -> Self {
        Object::new(&[]).expect("Failed to create `CustomButton`.")
    }

    pub fn from_category(
        model: &FeedListCategoryModel,
        model_index: &mut HashMap<Arc<FeedListItemID>, FeedListItemGObject>,
    ) -> Self {
        let gobject = Self::new();
        let imp = imp::FeedListItemGObject::from_instance(&gobject);

        *imp.id.write() = model.id.clone();
        *imp.label.write() = Arc::new(model.label.clone());
        imp.item_count.set(model.item_count as u32);
        imp.sort_index.set(model.sort_index);

        let list_store = ListStore::new(super::FeedListItemGObject::static_type());
        for child in &model.children {
            match child {
                FeedListItem::Feed(feed) => {
                    let item = FeedListItemGObject::from_feed(feed);
                    list_store.append(&item);
                    model_index.insert(item.id(), item);
                }
                FeedListItem::Category(category) => {
                    let item = FeedListItemGObject::from_category(category, model_index);
                    list_store.append(&item);
                    model_index.insert(item.id(), item);
                }
            }
        }
        imp.list_store.write().replace(list_store);

        gobject
    }

    pub fn from_feed(model: &FeedListFeedModel) -> Self {
        let gobject = Self::new();
        let imp = imp::FeedListItemGObject::from_instance(&gobject);

        *imp.id.write() = model.id.clone();
        *imp.label.write() = Arc::new(model.label.clone());
        imp.item_count.set(model.item_count as u32);
        imp.sort_index.set(model.sort_index);

        gobject
    }

    pub fn id(&self) -> Arc<FeedListItemID> {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.id.read().clone()
    }

    pub fn parent_id(&self) -> Arc<CategoryID> {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.parent_id.read().clone()
    }

    pub fn item_count(&self) -> u32 {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.item_count.get()
    }

    pub fn set_item_count(&self, item_count: u32) {
        self.set_property("item-count", item_count.to_value());
    }

    pub fn sort_index(&self) -> i32 {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.sort_index.get()
    }

    pub fn label(&self) -> Arc<String> {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.label.read().clone()
    }

    pub fn set_label(&self, label: &str) {
        self.set_property("label", label.to_value());
    }

    pub fn is_feed(&self) -> bool {
        let imp = imp::FeedListItemGObject::from_instance(self);
        match &**imp.id.read() {
            FeedListItemID::Feed(..) => true,
            _ => false,
        }
    }

    pub fn is_category(&self) -> bool {
        let imp = imp::FeedListItemGObject::from_instance(self);
        match &**imp.id.read() {
            FeedListItemID::Category(_) => true,
            _ => false,
        }
    }

    pub fn children_model(&self) -> Option<ListModel> {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.list_store.read().clone().map(|model| model.upcast::<ListModel>())
    }

    pub fn children_list_store(&self) -> Option<ListStore> {
        let imp = imp::FeedListItemGObject::from_instance(self);
        imp.list_store.read().clone()
    }

    pub fn remove_from_index(&self, model_index: &mut HashMap<Arc<FeedListItemID>, FeedListItemGObject>) {
        let imp = imp::FeedListItemGObject::from_instance(self);

        if let Some(list_store) = imp.list_store.write().as_ref() {
            let n_items = list_store.n_items();
            for i in 0..n_items {
                if let Some(item) = list_store.item(i) {
                    if let Ok(item_gobject) = item.downcast::<FeedListItemGObject>() {
                        item_gobject.remove_from_index(model_index);
                        model_index.remove(&item_gobject.id());
                    }
                }
            }
        }
    }
}
