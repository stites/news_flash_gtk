use crate::app::Action;
use crate::error_dialog::ErrorDialog;
use crate::util::{GtkUtil, Util};
use glib::SignalHandlerId;
use gtk4::{prelude::*, subclass::prelude::*};
use gtk4::{Button, CompositeTemplate, InfoBar, ResponseType, Stack};
use news_flash::NewsFlashError;
use parking_lot::RwLock;

mod imp {
    use std::sync::Arc;

    use super::*;
    use glib::subclass;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/newsflash/ui_templates/reset_page.ui")]
    pub struct ResetPage {
        #[template_child]
        pub reset_button: TemplateChild<Button>,
        #[template_child]
        pub cancel_button: TemplateChild<Button>,
        #[template_child]
        pub reset_stack: TemplateChild<Stack>,
        #[template_child]
        pub info_bar: TemplateChild<InfoBar>,
        #[template_child]
        pub details_button: TemplateChild<Button>,

        pub error_details_signal: Arc<RwLock<Option<SignalHandlerId>>>,
    }

    impl Default for ResetPage {
        fn default() -> Self {
            Self {
                reset_button: TemplateChild::default(),
                cancel_button: TemplateChild::default(),
                reset_stack: TemplateChild::default(),
                info_bar: TemplateChild::default(),
                details_button: TemplateChild::default(),

                error_details_signal: Arc::new(RwLock::new(None)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ResetPage {
        const NAME: &'static str = "ResetPage";
        type ParentType = gtk4::Box;
        type Type = super::ResetPage;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ResetPage {}

    impl WidgetImpl for ResetPage {}

    impl BoxImpl for ResetPage {}
}

glib::wrapper! {
    pub struct ResetPage(ObjectSubclass<imp::ResetPage>)
        @extends gtk4::Widget, gtk4::Box;
}

impl ResetPage {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[]).unwrap()
    }

    pub fn init(&self) {
        let imp = imp::ResetPage::from_instance(self);

        let reset_stack = imp.reset_stack.get();
        imp.reset_button.connect_clicked(move |button| {
            reset_stack.set_visible_child_name("reset_spinner");
            button.set_sensitive(false);
            Util::send(Action::ResetAccount);
        });

        imp.cancel_button.connect_clicked(move |_button| {
            Util::send(Action::CancelReset);
        });

        // setup infobar
        imp.info_bar.connect_close(|info_bar| {
            info_bar.set_revealed(false);
        });
        imp.info_bar.connect_response(|info_bar, response| {
            if let ResponseType::Close = response {
                info_bar.set_revealed(false);
            }
        });
    }

    pub fn reset(&self) {
        let imp = imp::ResetPage::from_instance(self);

        imp.reset_stack.set_visible_child_name("reset_label");
        imp.reset_button.set_sensitive(true);

        GtkUtil::disconnect_signal(imp.error_details_signal.write().take(), &imp.details_button.get());
    }

    pub fn error(&self, error: NewsFlashError) {
        self.reset();

        let imp = imp::ResetPage::from_instance(self);
        imp.error_details_signal
            .write()
            .replace(imp.details_button.connect_clicked(move |button| {
                let parent = GtkUtil::get_main_window(button)
                    .expect("MainWindow is not a parent of reset page error details button.");
                let dialog = ErrorDialog::new();
                dialog.set_transient_for(Some(&parent));
                dialog.set_error(&error);
            }));

        imp.info_bar.set_revealed(true);
    }
}
